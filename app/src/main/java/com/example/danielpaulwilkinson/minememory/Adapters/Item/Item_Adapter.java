package com.example.danielpaulwilkinson.minememory.Adapters.Item;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.R;

public class Item_Adapter extends CursorAdapter {

    private ImageProcessing ia;
    private AppConfig config;

    public Item_Adapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        ia = new ImageProcessing(context);
        config = new AppConfig();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(
                R.layout.items_listitem_small, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView TypeTextView = (TextView) view.findViewById(R.id.txtItemType);
        TextView XTextView = (TextView) view.findViewById(R.id.txtX);
        TextView YTextView = (TextView) view.findViewById(R.id.txtY);
        TextView ZTextView = (TextView) view.findViewById(R.id.txtZ);
        ImageView paths = (ImageView) view.findViewById(R.id.imgItem);
        TypeTextView.setText(cursor.getString(cursor.getColumnIndex(SQLite.ITEM_TYPE)));
        XTextView.setText("X: " + cursor.getString(cursor.getColumnIndex(SQLite.ITEM_X)));
        YTextView.setText("Y: " + cursor.getString(cursor.getColumnIndex(SQLite.ITEM_Y)));
        ZTextView.setText("Z: " + cursor.getString(cursor.getColumnIndex(SQLite.ITEM_Z)));
        paths.setImageBitmap(ia.loadImage(cursor.getString(cursor.getColumnIndex(SQLite.ITEM_PATH)),config.ImageListQuality));
    }
}


