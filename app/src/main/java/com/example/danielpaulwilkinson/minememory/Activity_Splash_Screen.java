package com.example.danielpaulwilkinson.minememory;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAdMob;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ColourProcessing;

public class Activity_Splash_Screen extends AppCompatActivity {
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash_screen);
        ColourProcessing ut = new ColourProcessing(getApplicationContext());
        ut.setStatusBar(Color.parseColor("#1B5E20"),this);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        int SPLASH_DISPLAY_LENGTH = 3000;
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(Activity_Splash_Screen.this, Activity_Home.class);
                Activity_Splash_Screen.this.startActivity(mainIntent);
                Activity_Splash_Screen.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
