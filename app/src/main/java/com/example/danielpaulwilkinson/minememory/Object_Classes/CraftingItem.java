package com.example.danielpaulwilkinson.minememory.Object_Classes;

public class CraftingItem {

    //region VARIABLES
    private String imagePathItem;
    private String getImagePathCraft;
    private String name;
    private String desc;
    private String resistance;
    private String stackable;
    private String flammable;
    private String experience;
    private String drops;
    private String date;
    private String type;
    //endregion

    //region GETTERS/SETTERS
    public String getType() {return type;}
    public void setType(String type) {
        this.type = type;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getImagePathItem() {
        return imagePathItem;
    }
    public void setImagePathItem(String imagePathItem) {
        this.imagePathItem = imagePathItem;
    }
    public String getGetImagePathCraft() {
        return getImagePathCraft;
    }
    public void setGetImagePathCraft(String getImagePathCraft) {this.getImagePathCraft = getImagePathCraft;}
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getResistance() {
        return resistance;
    }
    public void setResistance(String resistance) {
        this.resistance = resistance;
    }
    public String getStackable() {
        return stackable;
    }
    public void setStackable(String stackable) {
        this.stackable = stackable;
    }
    public String getFlamable() {
        return flammable;
    }
    public void setFlamable(String flamable) {
        this.flammable = flamable;
    }
    public String getExperience() {
        return experience;
    }
    public void setExperience(String experience) {
        this.experience = experience;
    }
    public String getDrops() {
        return drops;
    }
    public void setDrops(String drops) {
        this.drops = drops;
    }
    //endregion

    //region CONSTRUCTOR
    public CraftingItem(String imagePathItem, String getImagePathCraft, String name, String desc, String resistence, String stackable, String flamable, String experience, String drops, String date, String type) {
        this.imagePathItem = imagePathItem;
        this.getImagePathCraft = getImagePathCraft;
        this.name = name;
        this.desc = desc;
        this.resistance = resistence;
        this.stackable = stackable;
        this.flammable = flamable;
        this.experience = experience;
        this.drops = drops;
        this.date = date;
        this.type = type;
    }
    //endregion

}
