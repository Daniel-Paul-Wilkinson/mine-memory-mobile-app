package com.example.danielpaulwilkinson.minememory.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.danielpaulwilkinson.minememory.R;

public class IntentManager {

    //region FRAGMENTS
        public void RefreshFragment(Activity context, Fragment fragment){
            FragmentManager fm = context.getFragmentManager();
            fm.beginTransaction().replace(R.id.content_frame,  fragment).commit();
        }
    //this fragment required the ID so the refresh could refresh with the current selected location
    public void RefreshFragmentDetail(Activity context, Fragment fragment, String ID){
        FragmentManager fm = context.getFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("ID", String.valueOf(ID));
        fragment.setArguments(bundle);
        fm.beginTransaction().replace(R.id.content_frame,  fragment).commit();
    }
    public void newFragment(Activity context, Fragment fragment){
        FragmentManager fm = context.getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    //endregion

    //region INTENTS
        public void intent(Context context, Class to){
            Intent i = new Intent(context ,to)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
        public void IntentData(Context con, Class to, Bundle data){
            Intent i = new Intent(con ,to)
                       .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtras(data);
            con.startActivity(i);
        }
        //endregion
}
