package com.example.danielpaulwilkinson.minememory.Adapters.Item;

/**
 * Created by DanielPaulWilkinson on 30/05/2017.
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.danielpaulwilkinson.minememory.Activity_Detail.Activity_Item_Detail;
import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_Item_List;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ColourProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.DateProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;

import java.text.ParseException;
import java.util.Date;


public class Item_RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Cursor c;
    private ImageProcessing ia;
    private SQLite db;
    private AppConfig config;
    private IntentManager tm;
    private DateProcessing dateProcessing;
    private int viewType;


    public Item_RecyclerAdapter(Context context, Cursor c, int viewType) {
        this.context = context;
        this.c = c;
        this.viewType = viewType;
        ia = new ImageProcessing(context);
        db = new SQLite(context);
        config = new AppConfig();
        tm = new IntentManager();
        dateProcessing = new DateProcessing();
    }
    class ItemViewHolderSmall extends RecyclerView.ViewHolder {
        TextView XTextView;
        TextView YTextView;
        TextView ZTextView;
        TextView TypeTextView;
        ImageView paths ;
        ItemViewHolderSmall(View itemView) {
            super(itemView);
            TypeTextView = (TextView) itemView.findViewById(R.id.txtItemType);
            XTextView = (TextView) itemView.findViewById(R.id.txtX);
            YTextView = (TextView) itemView.findViewById(R.id.txtY);
            ZTextView = (TextView) itemView.findViewById(R.id.txtZ);
            paths = (ImageView) itemView.findViewById(R.id.imgItem);
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new ItemViewHolderSmall(LayoutInflater.from(context).inflate(
                    R.layout.items_listitem_small, parent, false));
        }else if(viewType == 1){
            return null;
        }else{
            return null;
        }
    }

    public void setViewType(int selectedviewType){
       viewType =  selectedviewType;
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!c.moveToPosition(position)) {
            return;
        }
        switch (holder.getItemViewType()) {
            case 0: {
                ItemViewHolderSmall view = (ItemViewHolderSmall)holder;
                view.TypeTextView.setText(c.getString(c.getColumnIndex(SQLite.ITEM_TYPE)));
                view.XTextView.setText("X: " + c.getString(c.getColumnIndex(SQLite.ITEM_X)));
                view.YTextView.setText("Y: " + c.getString(c.getColumnIndex(SQLite.ITEM_Y)));
                view.ZTextView.setText("Z: " + c.getString(c.getColumnIndex(SQLite.ITEM_Z)));
                view.paths.setImageBitmap(ia.loadImage(c.getString(c.getColumnIndex(SQLite.ITEM_PATH)), config.ImageListQuality));
                view.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bn = new Bundle();
                        bn.putString("ID", (String) view.getTag());
                        tm.IntentData(context, Activity_Item_Detail.class, bn);
                    }
                });
            }
            break;
            case 1:{}
            break;
        }
    }
    public int getItemCount () {
            return c.getCount();
    }
    public void swapCursor(Cursor newCursor) {
        if (c != null) {
            c.close();
        }
        c = newCursor;

        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }
}
