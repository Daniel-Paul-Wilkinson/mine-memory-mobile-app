package com.example.danielpaulwilkinson.minememory.Utils;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Database.SharedPreferences;
import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAdMob;
import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAnalytics;
import com.example.danielpaulwilkinson.minememory.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class BaseActivity extends AppCompatActivity{

    @Override
    public void onResume(){
        super.onResume();
        AndroidGoogleAnalytics application = (AndroidGoogleAnalytics) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Activity - "+getClass().getSimpleName());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
    public Tracker returnTracker(){
        AndroidGoogleAnalytics application = (AndroidGoogleAnalytics) getApplication();
        return application.getDefaultTracker();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();

    }
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
