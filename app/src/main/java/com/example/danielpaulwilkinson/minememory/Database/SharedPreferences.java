package com.example.danielpaulwilkinson.minememory.Database;

import android.content.Context;

import com.google.ads.interactivemedia.v3.api.Ad;

import java.util.ArrayList;

/**
 * Created by DanielPaulWilkinson on 03/06/2017.
 */

public class SharedPreferences {

    //region VARIABLES
    private Context con;
    private android.content.SharedPreferences sharedPreferences;
    //endregion

    //region CONSTRUCTOR
    public SharedPreferences(Context con){
        this.con = con;
    }
    //endregion

    //region SHARED-PREFERENCES-WORLD-ID
    public void setPreviouslyViewedWorld(String worldID) {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("WorldID", worldID).apply();
    }
    public String getPreviouslyViewedWorld() {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        return sharedPreferences.getString("WorldID", "");
    }
    //endregion


    public void setHaveVieweddAd(String bool) {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("bool", bool).apply();
    }
    public String getHaveViewedAd() {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        return sharedPreferences.getString("bool","");
    }

    public void setUserAccount(Boolean initAccount) {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean("initAccount", initAccount).apply();
    }
    public Boolean getUserAccount() {
        sharedPreferences = con.getSharedPreferences("Mine_Memory", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("initAccount",true);
    }
}
