package com.example.danielpaulwilkinson.minememory.Object_Classes;

public class WorldItem {

    //region VARIABLES
    private String x;
    private String y;
    private String z;
    private String lootTaken;
    private String locationType;
    private String desc;
    private String worldID  ;
    private String Date;
    private String ItemImagePath;
    //endregion

    //region GETTERS/SETTERS
    public String getItemImagePath() {
        return ItemImagePath;
    }
    public void setItemImagePath(String itemImagePath) {
        ItemImagePath = itemImagePath;
    }
    public String getLootTaken() {
        return lootTaken;
    }
    public void setLootTaken(String lootTaken) {
        this.lootTaken = lootTaken;
    }
    public String getWorldID() {
        return worldID;
    }
    public void setWorldID(String worldID) {
        this.worldID = worldID;
    }
    public String getDate() {
        return Date;
    }
    public void setDate(String date) {
        Date = date;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getLocationType() {
        return locationType;
    }
    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
    public String getX() {
        return x;
    }
    public void setX(String x) {
        this.x = x;
    }
    public String getY() {
        return y;
    }
    public    void setY(String y) {this.y = y;}
    public String getZ() {
        return z;
    }
    public void setZ(String z) {
        this.z = z;
    }
    //endregion

    //region CONSTRUCTOR
    public WorldItem( ){}
    public WorldItem(String x, String y, String z, String lootTaken, String locationType, String desc, String worldID, String date, String itemImagePath) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.lootTaken = lootTaken;
        this.locationType = locationType;
        this.desc = desc;
        this.worldID = worldID;
        Date = date;
        ItemImagePath = itemImagePath;
    }
    //endregion

}
