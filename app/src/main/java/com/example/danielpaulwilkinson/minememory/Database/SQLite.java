package com.example.danielpaulwilkinson.minememory.Database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.example.danielpaulwilkinson.minememory.Object_Classes.CraftingItem;
import com.example.danielpaulwilkinson.minememory.Object_Classes.User;
import com.example.danielpaulwilkinson.minememory.Object_Classes.World;
import com.example.danielpaulwilkinson.minememory.Object_Classes.WorldItem;

public class SQLite extends SQLiteOpenHelper{

    //region DATABASE-NAME-VERSION
    private static final String DATABASE_NAME = "Mine.db";
    private static final int DATABASE_VERSION = 7;
    //endregion

    //region WORLD-TABLE-STRINGS
    public static final String TABLE_WORLD = "worlds";
    public static final String WORLD_ID = "_id";
    public static final String WORLD_NAME = "worldName";
    public static final String WORLD_DATE = "worldDate";
    public static final String WORLD_DESC = "worldDesc";
    public static final String WORLD_SEED = "worldSeed";
    public static final String WORLD_IMAG = "worldImage";
    public static final String WORLD_DIFF = "worldDiff";
    //endregion

    //region ITEM-TABLE-STRINGS
    public static final String TABLE_ITEM = "location";
    public static final String ITEM_ID = "_id";
    public static final String ITEM_WORLD_ID = "world_id";
    public static final String ITEM_TYPE = "locationType";
    public static final String ITEM_DATE = "locationDate";
    public static final String ITEM_DESC = "locationDesc";
    public static final String ITEM_LOOT = "locationLoot";
    public static final String ITEM_PATH = "locationPath";
    public static final String ITEM_X = "locationX";
    public static final String ITEM_Y = "locationY";
    public static final String ITEM_Z = "locationZ";
    //endregion

    //region CRAFTING-TABLE-STRINGS
    public static final String TABLE_CRAFT = "craft";
    public static final String CRAFT_ID = "_id";
    public static final String CRAFT_NAME = "craftName";
    public static final String CRAFT_DESC = "craftDesc";
    public static final String CRAFT_RES = "craftResistence";
    public static final String CRAFT_IMAGE = "craftImage";
    public static final String CRAFT_IMAGE_ITEM = "craftImageItem";
    public static final String CRAFT_STACK = "craftStackable";
    public static final String CRAFT_FLAM = "craftflamable";
    public static final String CRAFT_EXP = "craftExp";
    public static final String CRAFT_DATE = "craftDate";
    public static final String CRAFT_DROP = "craftDrop";
    public static final String CRAFT_TYPE = "craftType";
    //endregion

    //region CURRENT-USER
    public static final String TABLE_USER = "user";
    public static final String USER_ID = "_id";
    public static final String USER_NAME = "userName";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_BIRTHDAY = "userBirthday";
    public static final String USER_CREATEDAT = "userJoinDate";
    public static final String USER_INSTAGRAM = "userInstargram";
    public static final String USER_FACEBOOK = "userFacebook";
    public static final String USER_TWITTER = "userTwitter";
    public static final String USER_IP = "userIP";
    public static final String USER_WORLDCOUNT = "userWorldCount";



    //endregion

    //region COLUMN-ARRAYS

    public static final String[] ALL_COLUMNS_USER =
            {
                    USER_ID,
                    USER_NAME,
                    USER_BIRTHDAY,
                    USER_IP,
                    USER_EMAIL,
                    USER_FACEBOOK,
                    USER_INSTAGRAM,
                    USER_TWITTER,
                    USER_CREATEDAT,
                    USER_WORLDCOUNT
            };



    public static final String[] ALL_COLUMNS_ITEM =
            {
                    ITEM_ID,
                    ITEM_WORLD_ID,
                    ITEM_TYPE,
                    ITEM_DATE,
                    ITEM_DESC,
                    ITEM_PATH,
                    ITEM_LOOT,
                    ITEM_X,
                    ITEM_Y,
                    ITEM_Z
            };
    public static final String[] ALL_COLUMNS_WORLD =
            {
                    WORLD_ID,
                    WORLD_NAME,
                    WORLD_DATE,
                    WORLD_DESC,
                    WORLD_SEED,
                    WORLD_IMAG,
                    WORLD_DIFF
            };

    public static final String[] ALL_COLUMNS_CRAFT =
            {
                    CRAFT_ID,
                    CRAFT_NAME,
                    CRAFT_DESC,
                    CRAFT_RES,
                    CRAFT_IMAGE,
                    CRAFT_IMAGE_ITEM,
                    CRAFT_STACK,
                    CRAFT_FLAM,
                    CRAFT_EXP,
                    CRAFT_DROP,
                    CRAFT_DATE,
                    CRAFT_TYPE

            };
    //endregion

    //region DATABASE-CONSTRUCTOR
    public SQLite(Context context) {super(context, DATABASE_NAME, null, DATABASE_VERSION);}
    //endregion

    //region ON-CREATE-UPDATE
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_USER =
                "CREATE TABLE IF NOT EXISTS " + TABLE_USER + " (" +
                        USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        USER_NAME + " TEXT, " +
                        USER_CREATEDAT + " TEXT, " +
                        USER_TWITTER + " TEXT, " +
                        USER_INSTAGRAM + " TEXT, " +
                        USER_FACEBOOK + " TEXT, " +
                        USER_EMAIL+ " TEXT, " +
                        USER_IP + " TEXT, " +
                        USER_WORLDCOUNT+ " TEXT, " +
                        USER_BIRTHDAY + " TEXT)";
        db.execSQL(CREATE_TABLE_USER);


        String CREATE_TABLE_CRAFT =
                "CREATE TABLE IF NOT EXISTS " + TABLE_CRAFT + " (" +
                        CRAFT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        CRAFT_NAME + " TEXT, " +
                        CRAFT_DESC + " TEXT, " +
                        CRAFT_RES + " TEXT, " +
                        CRAFT_IMAGE + " TEXT, " +
                        CRAFT_IMAGE_ITEM + " TEXT, " +
                        CRAFT_STACK + " TEXT, " +
                        CRAFT_FLAM + " TEXT, " +
                        CRAFT_EXP + " TEXT, " +
                        CRAFT_DATE + " TEXT, " +
                        CRAFT_TYPE + " TEXT, " +
                         CRAFT_DROP + " TEXT)";
        db.execSQL(CREATE_TABLE_CRAFT);

        String CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_WORLD + " (" +
                        WORLD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        WORLD_NAME + " TEXT, " +
                        WORLD_DATE + " TEXT, " +
                        WORLD_DESC + " TEXT, " +
                        WORLD_SEED + " TEXT, " +
                        WORLD_DIFF + " TEXT, " +
                        WORLD_IMAG + " TEXT)";
        db.execSQL(CREATE_TABLE);

        String CREATE_TABLE_ITEM =
                "CREATE TABLE IF NOT EXISTS " + TABLE_ITEM + " (" +
                        ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        ITEM_WORLD_ID + " TEXT, " +
                        ITEM_TYPE + " TEXT, " +
                        ITEM_DATE + " TEXT, " +
                        ITEM_DESC + " TEXT, " +
                        ITEM_PATH + " TEXT, " +
                        ITEM_X + " TEXT, " +
                        ITEM_Y + " TEXT, " +
                        ITEM_Z + " TEXT, " +
                        ITEM_LOOT + " TEXT)";
        db.execSQL(CREATE_TABLE_ITEM);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_WORLD);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ITEM);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CRAFT);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_USER);

        this.onCreate(db);
    }
    //endregion

    //region WORLD-CRUID-METHODS
    public void InsertWorld(World world) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(WORLD_NAME, world.getWorldName());
        values.put(WORLD_DESC, world.getWorldDesc());
        values.put(WORLD_DATE, world.getWorldDate());
        values.put(WORLD_SEED, world.getWorldSeed());
        values.put(WORLD_IMAG, world.getWorldImage());
        values.put(WORLD_DIFF, world.getWorldDiff());
        db.insert(TABLE_WORLD,
                null,
                values);
        db.close();
    }
    public World GetWorldById(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_WORLD, // a. table
                        ALL_COLUMNS_WORLD, // b. column names
                        SQLite.WORLD_ID +" = "+ id, // c. selections
                        null, // d. selections args null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit
        World world = null;
        if (cursor != null && cursor.moveToFirst()){
            world = new World();
            world.worldID = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_ID)));
            world.worldName = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_NAME)));
            world.worldDesc = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_DESC)));
            world.worldDate = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_DATE)));
            world.worldSeed = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_SEED)));
            world.worldDiff = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_DIFF)));
            world.worldImage = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.WORLD_IMAG)));
        }
        if (cursor != null) {
            cursor.close();
        }
        return world;
    }
    public void DeleteWorld(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_WORLD,
                WORLD_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.delete(TABLE_ITEM,
                ITEM_WORLD_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }
    public int UpdateWorld(World world, String ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(WORLD_NAME, world.getWorldName());
        values.put(WORLD_DESC, world.getWorldDesc());
        values.put(WORLD_DATE, world.getWorldDate());
        values.put(WORLD_SEED, world.getWorldSeed());
        values.put(WORLD_IMAG, world.getWorldImage());
        values.put(WORLD_DIFF, world.getWorldDiff());
        int i = db.update(TABLE_WORLD,
                values,
                WORLD_ID+" = ?",
                new String[] { String.valueOf(ID) });
        db.close();
        return i;
    }
    public Cursor WorldQuery(String[] columns, String selection,
                             String[] arguments, String having,
                             String orderBy, String limit) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor =
                db.query(TABLE_WORLD,
                        columns,
                        selection,
                        arguments,
                        null,
                        having,
                        orderBy,
                        limit

                );
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            return cursor;
        }
        return cursor;
    }
    //endregion

    //region WORLD-ITEM-CRUID-METHODS
    public void InsertWorldItem(WorldItem wc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ITEM_WORLD_ID, wc.getWorldID());
        values.put(ITEM_DATE, wc.getDate());
        values.put(ITEM_DESC, wc.getDesc());
        values.put(ITEM_PATH, wc.getItemImagePath());
        values.put(ITEM_TYPE, wc.getLocationType());
        values.put(ITEM_LOOT, wc.getLootTaken());
        values.put(ITEM_X, wc.getX());
        values.put(ITEM_Y, wc.getY());
        values.put(ITEM_Z, wc.getZ());
        db.insert(TABLE_ITEM,
                null,
                values);
        db.close();
    }

    //select * from TABLE WORLD
    public WorldItem GetWorldItemById(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_ITEM, // a. table
                        ALL_COLUMNS_ITEM, // b. column names
                        ITEM_ID +" = ?", // c. selections
                        new String[] { String.valueOf(id) }, // d. selections args null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit
        WorldItem WorldCoordinate = null;
        if (cursor != null && cursor.moveToFirst()){
            WorldCoordinate = new WorldItem();
            WorldCoordinate.setWorldID(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_WORLD_ID))));
            WorldCoordinate.setDate(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_DATE))));
            WorldCoordinate.setDesc(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_DESC))));
            WorldCoordinate.setLocationType(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_TYPE))));
            WorldCoordinate.setItemImagePath(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_PATH))));
            WorldCoordinate.setLootTaken(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_LOOT))));
            WorldCoordinate.setX(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_X))));
            WorldCoordinate.setY(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_Y))));
            WorldCoordinate.setZ(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_Z))));
        }
        if (cursor != null) {
            cursor.close();
        }
        return WorldCoordinate;
    }

    public void DeleteWorldItem(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ITEM,
                ITEM_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public int UpdateWorldItem(WorldItem wc, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ITEM_WORLD_ID, wc.getWorldID());
        values.put(ITEM_DATE, wc.getDate());
        values.put(ITEM_DESC, wc.getDesc());
        values.put(ITEM_PATH, wc.getItemImagePath());
        values.put(ITEM_TYPE, wc.getLocationType());
        values.put(ITEM_LOOT, wc.getLootTaken());
        values.put(ITEM_X, wc.getX());
        values.put(ITEM_Y, wc.getY());
        values.put(ITEM_Z, wc.getZ());


        int i = db.update(TABLE_ITEM,
                values,
                ITEM_ID +" = ?",
                new String[] { String.valueOf(id) });
        db.close();
        return i; }

    public Cursor WorldItemQuery(String[] columns, String selection,
                                 String[] arguments, String having,
                                 String orderBy, String limit) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_ITEM,
                        columns,
                        selection,
                        arguments,
                        null,
                        having,
                        orderBy,
                        limit
                );
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            return cursor;
        }
        return cursor;
    }
    public String GetWorldChildCount(String table, String ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count("+SQLite.ITEM_WORLD_ID+") FROM " + table + " WHERE "+SQLite.ITEM_WORLD_ID+" = "+ID;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();

        return String.valueOf(cursor.getInt(0));

    }
//endregion

    //region CRAFT-CRUID-METHODS
    public Cursor queryDBcraft(String[] columns, String selection,
                               String[] arguments, String having,
                               String orderBy, String limit) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_CRAFT,
                        columns,
                        selection,
                        arguments,
                        null,
                        having,
                        orderBy,
                        limit
                );
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            return cursor;
        }
        return cursor;
    }

    public void InsertCraftItem(CraftingItem c) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CRAFT_NAME, c.getName());
        values.put(CRAFT_DESC, c.getDesc());
        values.put(CRAFT_DATE, c.getDate());
        values.put(CRAFT_DROP, c.getDrops());
        values.put(CRAFT_EXP, c.getExperience());
        values.put(CRAFT_IMAGE_ITEM, c.getGetImagePathCraft());
        values.put(CRAFT_IMAGE, c.getImagePathItem());
        values.put(CRAFT_FLAM, c.getFlamable());
        values.put(CRAFT_STACK, c.getStackable());
        values.put(CRAFT_RES, c.getResistance());
        values.put(CRAFT_TYPE, c.getType());
        db.insert(TABLE_CRAFT,
                null,
                values);
        db.close();
    }

    //endregion

    //region DATABASE-TEST-CHECKS
    public Boolean dataExistsIn(String table) {
        Boolean hasdata = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + table;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            hasdata = true;
        } else {
            hasdata = false;
        }
        return hasdata;
    }
    //endregion

    //region USER-CRUID-METHODS
    public void InsertUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ID, user.getUserID());
        values.put(USER_NAME, user.getUserName());
        values.put(USER_EMAIL, user.getUserEmail());
        values.put(USER_BIRTHDAY, user.getUserBirthday());
        values.put(USER_IP, user.getUserIP());
        values.put(USER_CREATEDAT, user.getUserCreatedAt());
        values.put(USER_FACEBOOK, user.getUserFacebook());
        values.put(USER_INSTAGRAM, user.getUserInstagram());
        values.put(USER_TWITTER, user.getUserTwitter());
        values.put(USER_WORLDCOUNT, user.getUserWorldCount());
        db.insert(TABLE_USER,
                null,
                values);
        db.close();
    }
    public void updateWorldCountById(int id, int worldCount){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_WORLDCOUNT, worldCount);
        int i = db.update(TABLE_USER,
                values,
                ITEM_ID +" = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
    public User GetUserById(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =
                db.query(TABLE_USER, // a. table
                        ALL_COLUMNS_USER, // b. column names
                        USER_ID +" = ?", // c. selections
                        new String[] { String.valueOf(id) }, // d. selections args null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit
        User user = null;
        if (cursor != null && cursor.moveToFirst()){
            user = new User();
            user.setUserID(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_ID))));
            user.setUserEmail(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_EMAIL))));
            user.setUserBirthday(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_BIRTHDAY))));
            user.setUserCreatedAt(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_CREATEDAT))));
            user.setUserIP(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_IP))));
            user.setUserFacebook(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_FACEBOOK))));
            user.setUserInstagram(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_INSTAGRAM))));
            user.setUserTwitter(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_TWITTER))));
            user.setUserWorldCount(String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.USER_WORLDCOUNT))));
        }
        if (cursor != null) {
            cursor.close();
        }
        return user;
    }
    //endregion

    //region HOME-STATISTICS
    public String GetCount(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + table;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        return String.valueOf(cursor.getInt(0));

    }
    //endregion
}
