package com.example.danielpaulwilkinson.minememory.Database;

import java.util.ArrayList;

/**
 * Created by DanielPaulWilkinson on 03/06/2017.
 */

public class WorldPlaces {

    private ArrayList<String> Biomes = new ArrayList<>();
    private ArrayList<String> Surface = new ArrayList<>();
    private ArrayList<String> WaterBodies = new ArrayList<>(); //Mineshaft
    private ArrayList<String> LargeStructures = new ArrayList<>(); //strong hold
    private ArrayList<String> SmallStructures = new ArrayList<>(); //bones, villages, witch hut
    private ArrayList<String> buildings = new ArrayList<>(); //bones, villages, witch hut
    private ArrayList<String> TheNether = new ArrayList<>(); //bones, villages, witch hut
    private ArrayList<String> TheEnd = new ArrayList<>(); //bones, villages, witch hut

    public void populateList() {
        Biomes.add("Ice Plains");
        Biomes.add("Ice Plains Spikes");
        Biomes.add("Cold Taiga");
        Biomes.add("Cold Taiga M");
        Biomes.add("Frozen River");
        Biomes.add("Cold Beach");
        Biomes.add("Extreme Hills");
        Biomes.add("Extreme Hills M");
        Biomes.add("Extreme Hills+");
        Biomes.add("Extreme Hills+ M");
        Biomes.add("Taiga");
        Biomes.add("Taiga M");
        Biomes.add("Mega Taiga");
        Biomes.add("Mega Spruce Taiga");
        Biomes.add("Stone Beach");
        Biomes.add("Plains");
        Biomes.add("Sunflower Plains");
        Biomes.add("Forest");
        Biomes.add("Flower Forest");
        Biomes.add("Birch Forest");
        Biomes.add("Birch Forest M");
        Biomes.add("Roofed Forest");
        Biomes.add("Roofed Forest M");
        Biomes.add("Swampland");
        Biomes.add("Swampland M");
        Biomes.add("Jungle");
        Biomes.add("Jungle M");
        Biomes.add("Jungle Edge");
        Biomes.add("Jungle Edge M");
        Biomes.add("Roofed Forest M");
        Biomes.add("River");
        Biomes.add("Beach");
        Biomes.add("Mushroom Island");
        Biomes.add("Mushroom Island Shore");
        Biomes.add("The End");
        Biomes.add("Desert");
        Biomes.add("Desert M");
        Biomes.add("Savanna");
        Biomes.add("Savanna M");
        Biomes.add("Mesa");
        Biomes.add("Mesa (Bryce)");
        Biomes.add("Mesa Plateau F");
        Biomes.add("Mesa Plateau F M");
        Biomes.add("Plateau");
        Biomes.add("Plateau M");
        Biomes.add("Mesa Plateau F M");
        Biomes.add("The Nether");
        Biomes.add("Plateau M");
        Biomes.add("Ocean");
        Biomes.add("Deep Ocean");
        Biomes.add("The Void");
        Biomes.add("Hills");
        Biomes.add("Frozen Ocean");
        Biomes.add("Extreme Hills Edge");
        Surface.add("Basin");
        WaterBodies.add("Ocean");
        WaterBodies.add("Lake");
        WaterBodies.add("River");
        LargeStructures.add("Cavern");
        LargeStructures.add("Stronghold");
        LargeStructures.add("Abandoned Mineshaft");
        LargeStructures.add("Ravine");
        SmallStructures.add("Dungeon");
        SmallStructures.add("Mineral Vein");
        SmallStructures.add("Tree");
        SmallStructures.add("Huge Mushroom");
        SmallStructures.add("Spring");
        SmallStructures.add("Dessert Well");
        SmallStructures.add("Moss Stone Bolder");
        SmallStructures.add("Ice Spike");
        SmallStructures.add("Fossil");
        buildings.add("Village");
        buildings.add("Desert Temple");
        buildings.add("Jungle Temple");
        buildings.add("Witch Hut");
        buildings.add("Ocean Monument");
        buildings.add("Igloo");
        buildings.add("Woodland Mansion");
        TheNether.add("Lava Sea");
        TheNether.add("Nether Fortress");
        TheNether.add("Glowstone Cluster");
        TheNether.add("Other");
        TheEnd.add("The Central Island");
        TheEnd.add("Obsidian Pillar");
        TheEnd.add("Obsidian Platform");
        TheEnd.add("End Fountin");
        TheEnd.add("End Gateway Portal");
        TheEnd.add("End City");
        TheEnd.add("End Ship");
        TheEnd.add("Chorus Tree");
        TheEnd.add("End Portal");
    }

    public ArrayList returnTerrains() {
        return Biomes;
    }

    public ArrayList returnSurface() {
        return Surface;
    }

    public ArrayList returnWaterBody() {
        return WaterBodies;
    }

    public ArrayList returnLargeStructure() {
        return LargeStructures;
    }

    public ArrayList returnSmallStructure() {
        return SmallStructures;
    }

    public ArrayList returnBuildings() {
        return buildings;
    }

    public ArrayList returnTheNether() {
        return TheNether;
    }

    public ArrayList returnTheEnd() {
        return TheEnd;
    }

}
