package com.example.danielpaulwilkinson.minememory.Activity_Lists;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.danielpaulwilkinson.minememory.Activity_Add.Activity_Add_Item;
import com.example.danielpaulwilkinson.minememory.Activity_Detail.Activity_Item_Detail;
import com.example.danielpaulwilkinson.minememory.Adapters.Item.Item_Adapter;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Database.SharedPreferences;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;

/**
 * Created by DanielPaulWilkinson on 01/06/2017.
 */

public class Activity_Item_List extends BaseActivity {
    Cursor c;
    CursorAdapter ca;
    private SQLite db;
    ListView lstWorldItems;
    IntentManager tm;
    ImageProcessing im;
    SharedPreferences sharedPreferences;
    String ID;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.item_list);
        lstWorldItems = (ListView) findViewById(R.id.lstCoords);
        db = new SQLite(getApplicationContext());
        tm = new IntentManager();
        sharedPreferences = new SharedPreferences(getApplicationContext());
        im = new ImageProcessing(getApplicationContext());
        final Intent intent = getIntent();
        if (intent.hasExtra("ID")) {
            ID = intent.getStringExtra("ID");
            sharedPreferences.setPreviouslyViewedWorld(ID);
        } else {
            ID = sharedPreferences.getPreviouslyViewedWorld();
        }
        setTitle(db.GetWorldById(ID).getWorldName());
        LoadAllWorldslocations("", "", ID);


        lstWorldItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);
                cursor.moveToPosition(position);
                final String itemID = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_ID)));
                Intent in = new Intent(getApplicationContext(),Activity_Item_Detail.class);
                in.putExtra("ID",ID);
                in.putExtra("ItemID",itemID);
                startActivity(in);

            }
        });
        lstWorldItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);
                cursor.moveToPosition(position);
                final String itemID = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_ID)));
                final String itemName = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(SQLite.ITEM_TYPE)));
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Item_List.this);
                alertDialogBuilder.setTitle("Delete");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete " + itemName)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteWorldLocation(String.valueOf(itemID));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .setNeutralButton("Return", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            }

        });

        lstWorldItems.setEmptyView(findViewById(R.id.empty_listitem));


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem search = menu.findItem(R.id.menuSearch);
        MenuItem add = menu.findItem(R.id.add);
        MenuItem desc = menu.findItem(R.id.desc);
        MenuItem asc = menu.findItem(R.id.asc);
        desc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllWorldslocations(""," DESC ",ID);
                return false;
            }
        });
        asc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllWorldslocations(""," ASC ",ID);

                return false;

            }
        });
        add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Bundle bn = new Bundle();
                bn.putString("IDs",ID);
                tm.IntentData(Activity_Item_List.this, Activity_Add_Item.class,bn);
                return true;
            }
        });
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                LoadAllWorldslocations(newText,"",ID);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void LoadAllWorldslocations(String name, String order, String id){
        new LoadWorldLocations().execute(name,order,id);

    }
    private void deleteWorldLocation(String deleteID) {
        im.deleteImage(db.GetWorldItemById(deleteID).getItemImagePath());
        db.DeleteWorldItem(deleteID);
        LoadAllWorldslocations("","",ID);

    }

    private class LoadWorldLocations extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... params) {
            String Like = "";
            String Order = "";
            String three = "";
            if(params[0] != null) {
                Like = params[0];
            }
            if(params[1] != null){
                Order = params[1];
            }
            if(params[2] != null){
                three = params[2];
            }
            c = db.WorldItemQuery(SQLite.ALL_COLUMNS_ITEM, SQLite.ITEM_TYPE+" LIKE '%"+Like+"%'" +" AND "+ SQLite.ITEM_WORLD_ID+" = "+three ,null,null, SQLite.ITEM_TYPE +" "+Order ,null);
            ca = new Item_Adapter(Activity_Item_List.this,c,0);
            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            lstWorldItems.setAdapter(ca);
        }
    }
}




