package com.example.danielpaulwilkinson.minememory.Utils;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.danielpaulwilkinson.minememory.R;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static android.provider.Settings.Global.getString;

public class FormValidation {

    //region BASIC-FORM-VALIDATION
    public static Boolean basicEmptyStringValidation(EditText element, String errorMessage) {
        Boolean isEmpty = true;
        if (TextUtils.isEmpty(element.getText())) {
            element.setError(errorMessage);
            return isEmpty = false;
        }
        return true;
    }
    public static boolean isValidEmailId(EditText element, HashMap<String, String> errorMessages){

         if(TextUtils.isEmpty(element.getText())){
             element.setError(errorMessages.get("Empty"));
             return false;
         }
         else if (Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                 + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                 + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                 + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                 + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                 + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(element.getText()).matches())
         {
             element.setError(errorMessages.get("Oops, please enter a correct email format!"));
             return false;
         }
        return true;
    }

    public static Boolean basicEmptySpinnerValidation(Spinner diffSpinner, String s) {
       Boolean hasValue = false;
        if(diffSpinner.getSelectedItem() != null){
            hasValue = true;
        }
        return hasValue;
    }
    //endregion

}