package com.example.danielpaulwilkinson.minememory.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;

/**
 * Created by DanielPaulWilkinson on 08/06/2017.
 */

public class Crafting_Adapter extends CursorAdapter {


    private ImageProcessing ia;
    private AppConfig config;

    public Crafting_Adapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        ia = new ImageProcessing(context);
        config = new AppConfig();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(
                R.layout.crafting_listitem_small, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView tvName = (ImageView) view.findViewById(R.id.imgcraft);
        TextView name = (TextView) view.findViewById(R.id.txtCraftName);
        tvName.setImageBitmap(ia.loadImage(cursor.getString(cursor.getColumnIndex(SQLite.CRAFT_IMAGE_ITEM)),config.ImageListQuality));
        name.setText(cursor.getString(cursor.getColumnIndex(SQLite.CRAFT_NAME)));
    }
}
