package com.example.danielpaulwilkinson.minememory.Activity_Detail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Database.SharedPreferences;
import com.example.danielpaulwilkinson.minememory.Object_Classes.WorldItem;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.DateProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;

import java.text.ParseException;
import java.util.Date;

public class Activity_Item_Detail extends BaseActivity {

    //region VARIABLES
    private Cursor c;
    private SQLite db;
    private String ItemID;
    private ImageProcessing im;
    private AppConfig config;
    DateProcessing dt;
    WorldItem wc;
    TextView txtdate;
    EditText txtDesc;
    ImageView imgView;
    EditText txtName;
    SurfaceView sv;
    EditText txtX;
    EditText txtY;
    EditText txtZ;
    //endregion

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_coordinate_detail);

        //region CLASSES
        db = new SQLite(getApplicationContext());
        im = new ImageProcessing(getApplicationContext());
        SharedPreferences sharedPreferences = new SharedPreferences(getApplicationContext());
        dt = new DateProcessing();
        config = new AppConfig();
        //endregion

        //region FORM-FIELDS
         txtdate =   (TextView) findViewById(R.id.txtItemCreatedDate);
         txtDesc =   (EditText) findViewById(R.id.etItemNote);
         imgView =   (ImageView) findViewById(R.id.imgItem);
         txtName =    (EditText) findViewById(R.id.etItemName);
         sv =     (SurfaceView) findViewById(R.id.svDate);
         txtX =      (EditText) findViewById(R.id.etItemX);
         txtY =         (EditText) findViewById(R.id.etItemY);
         txtZ =      (EditText) findViewById(R.id.etItemZ);
        //endregion

        //region INTENT-DATA
        Intent intent = getIntent();
        String worldID;
        if (intent.hasExtra("WorldID")) {
            worldID = intent.getStringExtra("WorldID");
            sharedPreferences.setPreviouslyViewedWorld(worldID);
        } else {
            worldID = sharedPreferences.getPreviouslyViewedWorld();
        }
        if (intent.hasExtra("ItemID")) {
            ItemID = intent.getStringExtra("ItemID");
        }
        //endregion

        //region RUN-ASYNC-LOAD-DETAIL
        LoadItemDetail asyncClass = new LoadItemDetail();
        asyncClass.execute(ItemID);
        //endregion

    }

    //region ASYNC-LOAD-DETAIL-CLASS
    @SuppressLint("StaticFieldLeak")
    private class LoadItemDetail extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... params) {
            wc = db.GetWorldItemById(params[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);

            try {
                txtdate.setText(dt.calculateTimeSincePost(new Date(), dt.mySQLDateFormat.parse(wc.getDate())));
            } catch (ParseException e) {
                Log.e("ERROR","Parse error at async method");
            }
            setTitle(wc.getLocationType());
            sv.setBackgroundColor(Color.parseColor("#6c6c6c"));
            imgView.setImageBitmap(im.loadImage(wc.getItemImagePath(),config.ImageDetailQuality));
            txtName.setText(wc.getLocationType());
            txtDesc.setText(wc.getDesc());
            txtX.setText(wc.getX());
            txtY.setText(wc.getY());
            txtZ.setText(wc.getZ());
            db.close();
        }
    }
    //endregion

}

