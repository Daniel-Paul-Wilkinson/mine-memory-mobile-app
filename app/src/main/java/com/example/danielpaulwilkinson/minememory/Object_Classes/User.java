package com.example.danielpaulwilkinson.minememory.Object_Classes;

public class User {

    private String userID;
    private String userName;
    private String userBirthday;
    private String userEmail;
    private String userIP;
    private String userCreatedAt;
    private String userWorldThreshold;
    private String userInstagram;
    private String userTwitter;
    private String userFacebook;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    public String getUserCreatedAt() {
        return userCreatedAt;
    }

    public void setUserCreatedAt(String userCreatedAt) {
        this.userCreatedAt = userCreatedAt;
    }

    public String getUserWorldCount() {
        return userWorldThreshold;
    }

    public void setUserWorldCount(String userWorldCount) {
        this.userWorldThreshold = userWorldCount;
    }

    public String getUserInstagram() {
        return userInstagram;
    }

    public void setUserInstagram(String userInstagram) {
        this.userInstagram = userInstagram;
    }

    public String getUserTwitter() {
        return userTwitter;
    }

    public void setUserTwitter(String userTwitter) {
        this.userTwitter = userTwitter;
    }

    public String getUserFacebook() {
        return userFacebook;
    }

    public void setUserFacebook(String userFacebook) {
        this.userFacebook = userFacebook;
    }

    public User(){}

    public User(String userID, String userName, String userBirthday, String userEmail, String userIP, String userCreatedAt, String userWorldThreshold, String userInstagram, String userTwitter, String userFacebook) {
        this.userID = userID;
        this.userName = userName;
        this.userBirthday = userBirthday;
        this.userEmail = userEmail;
        this.userIP = userIP;
        this.userCreatedAt = userCreatedAt;
        this.userWorldThreshold = userWorldThreshold;
        this.userInstagram = userInstagram;
        this.userTwitter = userTwitter;
        this.userFacebook = userFacebook;
    }
}
