package com.example.danielpaulwilkinson.minememory.Activity_Lists;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.danielpaulwilkinson.minememory.Adapters.Crafting_Adapter;
import com.example.danielpaulwilkinson.minememory.Database.AllCraftingItems;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Object_Classes.CraftingItem;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.ColourProcessing;

import java.util.ArrayList;

/**
 * Created by DanielPaulWilkinson on 06/06/2017.
 */

public class Activity_Crafting_List extends BaseActivity {

    ColourProcessing ut;
    ListView lstCraft;
    Cursor c;
    CursorAdapter ca;
    AllCraftingItems cl;
    SQLite db;
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.crafting_list);
        cl = new AllCraftingItems(getApplicationContext());
        lstCraft = (ListView) findViewById(R.id.lstCraft);
        setTitle("Crafting_Adapter Manual");
        db = new SQLite(getApplicationContext());
        ut = new ColourProcessing(getApplicationContext());
        LoadAllCraftable("", "", "");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_craft,menu);
        MenuItem search = menu.findItem(R.id.menuSearch);
        final MenuItem add = menu.findItem(R.id.add);
        MenuItem desc = menu.findItem(R.id.desc);
        MenuItem asc = menu.findItem(R.id.asc);

        final MenuItem utility = menu.findItem(R.id.utilitty);
        MenuItem mech = menu.findItem(R.id.mechanisms);
        MenuItem tool = menu.findItem(R.id.tools);


        utility.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllCraftable("","","ColourProcessing");
                return false;
            }
        });
        mech.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllCraftable("","","Mechanisms");
                return false;

            }
        });
        tool.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllCraftable("","","Tool");
                return false;

            }
        });

        desc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllCraftable(""," DESC ","");
                return false;
            }
        });
        asc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllCraftable(""," ASC ","");
                return false;

            }
        });
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                LoadAllCraftable(newText,"","");

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void LoadAllCraftable(String name, String order,String type){
        new LoadAllCraftable().execute(name,order,type);

    }
    private class LoadAllCraftable extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... params) {
            String Like = "";
            String Order = "";
            String Type = "";
            if(params[0] != null) {
                Like = params[0];
            }
            if(params[1] != null){
                Order = params[1];
            }
            if(params[2] != null){
                Type = params[2];
            }
if(!db.dataExistsIn(SQLite.TABLE_CRAFT)){
    ArrayList<CraftingItem> arrayOfUsers = cl.ReturnList();
    for(CraftingItem craftingItem: arrayOfUsers){

        db.InsertCraftItem(craftingItem);
    }
}
    c = db.queryDBcraft(SQLite.ALL_COLUMNS_CRAFT, SQLite.CRAFT_NAME+" LIKE '%"+Like+"%' AND "+ SQLite.CRAFT_TYPE +" LIKE '%"+Type+"%'" ,null,null, SQLite.CRAFT_TYPE +" "+Order ,null);
ca = new Crafting_Adapter(Activity_Crafting_List.this,c,0);
            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            lstCraft.setAdapter(ca);
        }
    }
}
