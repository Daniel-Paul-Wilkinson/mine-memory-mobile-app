package com.example.danielpaulwilkinson.minememory.Utils;

import android.content.Context;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Database.SharedPreferences;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.Logger;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.LoggerList;
import com.example.danielpaulwilkinson.minememory.Object_Classes.User;

public class UserProcessing {

    Context contexts;


    public UserProcessing(Context context){
        contexts = context;
    }

    public static void InitAccount(SharedPreferences sp, SQLite sql){
        Boolean firstTime = sp.getUserAccount();
        if (firstTime) {
            LoggerList.addLogItem(new Logger("DEBUG MODE"," InitAccount() ACCOUNT CREATED "));
            sql.InsertUser(new User(
                    "1",
                    "AppUser",
                    new DateProcessing().mySQLDateFormat.format(new java.util.Date()),
                    "danielpaulwilkinson.developer+user@gmail.com",
                    "10.000.000",
                    new DateProcessing().mySQLDateFormat.format(new java.util.Date()),
                    AppConfig.setAdThreshold,
                    "",
                    "",
                    ""));
            sp.setUserAccount(false);
        } else{
            LoggerList.addLogItem(new Logger("DEBUG MODE","InitAccount() ACCOUNT EXISTS "));
        }
    }
}
