package com.example.danielpaulwilkinson.minememory.Object_Classes;

public class World {

    //region VARIABLES
    public String worldName;
    public String worldDesc;
    public String worldDate;
    public String worldSeed;
    public String worldImage;
    public String worldDiff;
    public String worldID;
    //endregion

    //region GETTERS/SETTERS
    public String getWorldDiff() {
        return worldDiff;
    }
    public void setWorldDiff(String worldDiff) {
        this.worldDiff = worldDiff;
    }
    public String getWorldID() {
        return worldID;
    }
    public void setWorldID(String worldID) {
        this.worldID = worldID;
    }

    public String getWorldName() {
        return worldName;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    public String getWorldDesc() {
        return worldDesc;
    }

    public void setWorldDesc(String worldDesc) {
        this.worldDesc = worldDesc;
    }

    public String getWorldDate() {
        return worldDate;
    }

    public void setWorldDate(String worldDate) {
        this.worldDate = worldDate;
    }

    public String getWorldSeed() {
        return worldSeed;
    }

    public void setWorldSeed(String worldSeed) {
        this.worldSeed = worldSeed;
    }

    public String getWorldImage() {
        return worldImage;
    }

    public void setWorldImage(String worldImage) {
        this.worldImage = worldImage;
    }
    //endregion

    //region CONSTRUCTOR
    public  World(){}
    public World( String worldName, String worldDesc, String worldDate, String worldSeed, String worldImage, String worldDiff) {
        this.worldName = worldName;
        this.worldDesc = worldDesc;
        this.worldDate = worldDate;
        this.worldSeed = worldSeed;
        this.worldImage = worldImage;
        this.worldDiff = worldDiff;
    }
    //endregion

}
