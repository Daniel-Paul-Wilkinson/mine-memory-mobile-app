package com.example.danielpaulwilkinson.minememory.Activity_Add;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_World_List;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAnalytics;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.DateProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.FormValidation;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;
import com.example.danielpaulwilkinson.minememory.Object_Classes.World;
import com.example.danielpaulwilkinson.minememory.R;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

public class Activity_Add_World extends BaseActivity {

    //TODO: LOOK INTO ADD WORLD DESIGN...
    //TODO: found that detial also requires random image selextion button

    //region VARIABLES
    private final int REQUEST_CODE_GET_CAMERA_IMAGE = 1, REQUEST_CODE_GET_GALLERY_IMAGE = 2, REQEST_CODE_SELECTED_IMAGE = 3;
    private ImageView imgWorld;
    private EditText txtName, txtDesc, txtSeed;
    private Button btnSubmitWorld, btnUpload ,btnChangeimage;
    private SQLite db;
    private DateProcessing date;
    private ImageProcessing is;
    private IntentManager tm;
    private AppConfig config;
    private Animation an;
    private Async_InsertWorld async_insertWorld;
    private Async_LoadBitmap populateImageViews;
    private Async_UpdateWorld async_updateWorld;
    private String ID;
    private String Type = "New";
    private Spinner diffSpinner;

    //endregion

    //region ONCREATE
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_add_world);
        setTitle("Add A World");
        db = new SQLite(Activity_Add_World.this);
        date = new DateProcessing();
        config = new AppConfig();
        is = new ImageProcessing(Activity_Add_World.this);
        tm = new IntentManager();
        an = new AlphaAnimation(0.5f, 1);
        an.setDuration(2000);

        async_insertWorld = new Async_InsertWorld();
        async_updateWorld = new Async_UpdateWorld();
        populateImageViews = new Async_LoadBitmap();

        btnSubmitWorld = (Button) findViewById(R.id.btnSubmitWorld);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        txtName = (EditText) findViewById(R.id.txtxlworldname);
        txtDesc = (EditText) findViewById(R.id.txtDesc);
        txtSeed = (EditText) findViewById(R.id.txtSeed);
        diffSpinner = (Spinner) findViewById(R.id.diffSpinner);
        imgWorld = (ImageView) findViewById(R.id.imgWorld);
        btnChangeimage = (Button) findViewById(R.id.btnChangeimage);


        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidGoogleAnalytics.CreateEvent(returnTracker(),"Image-Upload","Uploaded New Image");
                btnUpload.startAnimation(an);
                CameraModel();
            }
        });
        btnSubmitWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSubmitWorld.startAnimation(an);
                AddNewWorld();
                btnSubmitWorld.setEnabled(false);
            }
        });
        btnChangeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AndroidGoogleAnalytics.CreateEvent(returnTracker(),"Image-Change","Change Provided Image");
                btnChangeimage.startAnimation(an);
                imgWorld.setImageBitmap(is.returnRandomMinecraftImage());

            }
        });
        //if we are using this activity for a new world then we can generate a new image for the image view.
        if(Type.equals("New")) {
            populateImageViews.execute();
        }

        //set up spinner and values so we can load them in
        setupSpinner();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras != null) {
            if (extras.get("ID") != null && extras.get("Type") != null) {
                ID = extras.getString("ID");
                Type = extras.getString("Type");
                populateFields(ID);
                ChangeButtonText();
            }
        }

    }

    private void setupSpinner(){
        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.world_spinner_difficulty,AppConfig.returnWorldDifficulties()
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.world_spinner_difficulty);
        diffSpinner.setAdapter(spinnerArrayAdapter);
    }
    private void ChangeButtonText() {
        btnSubmitWorld.setText("Save Changes");
        setTitle("Update World");
    }
    private void populateFields(String ID) {
        World w = db.GetWorldById(ID);
        txtName.setText(w.getWorldName());
        Log.e("VALUE",w.getWorldDiff());
        diffSpinner.setSelection(AppConfig.returnWorldDifficulties().indexOf(w.getWorldDiff()));
        txtSeed.setText(w.getWorldSeed());
        txtDesc.setText(w.getWorldDesc());
        imgWorld.setImageBitmap(is.loadImage(w.getWorldImage(),AppConfig.ImageDetailQuality));
    }
    //endregion

    //region ADD-NEW-WORLD
    private void AddNewWorld() {
        Boolean nameIsValid = FormValidation.basicEmptyStringValidation(txtName, "Please enter a world name.");
        Boolean descIsValid = FormValidation.basicEmptyStringValidation(txtDesc, "Please enter a world description.");
        Boolean seedIsValid = FormValidation.basicEmptyStringValidation(txtSeed, "Please enter a world seed.");
        Boolean diffIsValid = FormValidation.basicEmptySpinnerValidation(diffSpinner, "Please enter a world difficulty.");
        Log.e("VALUE",diffSpinner.getSelectedItem().toString());

        if (nameIsValid && descIsValid && seedIsValid && diffIsValid) {
            Bitmap bm = ((BitmapDrawable) imgWorld.getDrawable()).getBitmap();
            if(Type.equals("New")) {
                async_insertWorld.execute(new World(
                        txtName.getText().toString().trim(),
                        txtDesc.getText().toString().trim(),
                        date.mySQLDateFormat.format(new Date()),
                        txtSeed.getText().toString().trim(),
                        is.SaveImageToInternalStorage("Mine_Memory", bm),
                        diffSpinner.getSelectedItem().toString().trim()
                ));
            }else if (Type.equals("Edit")){
                World w = db.GetWorldById(ID);
                Boolean isDeleted = is.deleteImage(w.getWorldImage());
                if(isDeleted) {
                    async_updateWorld.execute(new World(
                            //remember to add world ID... in async method...
                            txtName.getText().toString().trim(),
                            txtDesc.getText().toString().trim(),
                            date.mySQLDateFormat.format(new Date()),
                            txtSeed.getText().toString().trim(),
                            is.SaveImageToInternalStorage("Mine_Memory", bm),
                            diffSpinner.getSelectedItem().toString().trim()
                    ));
                }
            }
        }
    }
    //endregion

    //region IMAGE-SELECT
    private void CameraModel() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Add_World.this);
        alertDialogBuilder.setTitle("Image Selection");
        alertDialogBuilder
                .setMessage("Where would you like to get an image from?")
                .setCancelable(false)
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startCamera();
                    }
                })
                .setNegativeButton("Libary", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startGallery();
                    }
                })
                .setNeutralButton("Return", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_GET_CAMERA_IMAGE);
    }
    private void startGallery() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_CODE_GET_GALLERY_IMAGE);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQEST_CODE_SELECTED_IMAGE);
            }
        }
    }
    //endregion

    //region RESULT-AND-PERMISSION
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_GET_GALLERY_IMAGE:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        InputStream imageStream = null;
                        Uri selectedImage = data.getData();
                        imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImage);
                        Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                        //txtFileName.setText("Uploaded: "+ImageProcessing.getFileName(selectedImage,getApplicationContext()));
                        imgWorld.setImageBitmap(yourSelectedImage);
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_CODE_GET_CAMERA_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        imgWorld.setImageBitmap(photo);
                        //txtFileName.setText("New Image: "+ is.generateName("uploaded-image",false));
                }
                break;
            default:
                break;
        }
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQEST_CODE_SELECTED_IMAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startGallery();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "This feature requires an image from your storage", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    //endregion

    //region ASYNC-METHODS
    @SuppressLint("StaticFieldLeak")
    private class Async_InsertWorld extends AsyncTask<World, Void, Void> {
        @Override
        protected Void doInBackground(World... world) {
            db.InsertWorld(world[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            Toast.makeText(Activity_Add_World.this, "World Added", Toast.LENGTH_SHORT).show();
            tm.intent(Activity_Add_World.this, Activity_World_List.class);
            btnSubmitWorld.setEnabled(true);

        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Async_UpdateWorld extends AsyncTask<World, Void, Void> {
        @Override
        protected Void doInBackground(World... world) {
            db.UpdateWorld(world[0],ID);

            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            Toast.makeText(Activity_Add_World.this, "World Updated", Toast.LENGTH_SHORT).show();
            tm.intent(Activity_Add_World.this, Activity_World_List.class);
            btnSubmitWorld.setEnabled(true);


        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Async_LoadBitmap extends AsyncTask<Void, Void, Void> {

        Bitmap b;

        @Override
        protected Void doInBackground(Void... voids) {
            b = is.returnRandomMinecraftImage();
            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            imgWorld.setImageBitmap(b);
        }
    }
    //endregion


}

