package com.example.danielpaulwilkinson.minememory.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;

import java.util.concurrent.ConcurrentLinkedDeque;

public class ColourProcessing {

    //region VARIABLES
    private Context con;
    //endregion

    //region CONSTRUCTOR
    public ColourProcessing(Context con){
        this.con = con;
    }
    //endregion

    //region APP-COLOUR
    public void setStatusBar(int status, Activity activity) {
        Window window;
        if(activity != null){
            window = activity.getWindow();
        } else{
            window = ((Fragment) null).getActivity().getWindow();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(status);
        }
    }
    public void setActionBar(int Action, Activity activity, Fragment fragment) {

        if(activity != null){
            ((AppCompatActivity) activity).getSupportActionBar().setBackgroundDrawable(
                    new ColorDrawable(Action));
        } else{
            fragment.getActivity().getActionBar().setBackgroundDrawable(
                    new ColorDrawable(Action));
        }
    }
    //endregion


    public static String  returnDiffColour(String s){

        String colour = "#FFFFFF";

        if("Easy".equals(s)){
            colour = "#7CB342";
        }
        else if("Normal".equals(s)){
            colour = "#FB8C00";
        }
        else if("Hard".equals(s)){
            colour = "#E53935";
        }
        else if("Hardcore".equals(s)){
            colour = "#DD2C00";
        }
        return colour;
    }

}
