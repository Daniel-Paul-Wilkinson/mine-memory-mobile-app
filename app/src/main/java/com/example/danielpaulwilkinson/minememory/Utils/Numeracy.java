package com.example.danielpaulwilkinson.minememory.Utils;

import android.content.Context;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.example.danielpaulwilkinson.minememory.R;

public class Numeracy {

    public static Boolean isMultipleof5 (int n)
    {
        while ( n > 0 )
            n = n - 5;
        if ( n == 0 )
            return true;
        return false;
    }}
