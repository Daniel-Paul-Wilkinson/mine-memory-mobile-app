package com.example.danielpaulwilkinson.minememory.Activity_Lists;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.example.danielpaulwilkinson.minememory.Activity_Add.Activity_Add_World;
import com.example.danielpaulwilkinson.minememory.Adapters.World.World_RecyclerAdapter;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAdMob;
import com.example.danielpaulwilkinson.minememory.Object_Classes.User;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;
public class Activity_World_List extends BaseActivity {

    private RecyclerView RecycleListWorlds;
    private World_RecyclerAdapter wra;
    private SQLite db;
    private IntentManager tm;
    private AndroidGoogleAdMob adMob;
    private AppConfig config;
    private String listViewLimit = "10";
    private Paint p = new Paint();

    @SuppressLint("ClickableViewAccessibility")
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.world_list);
        setTitle("Your Worlds");
        db = new SQLite(getApplicationContext());
        tm = new IntentManager();
        adMob = new AndroidGoogleAdMob(getApplicationContext(), db);
        config = new AppConfig();

        //region recycler View
        RecycleListWorlds = (RecyclerView) findViewById(R.id.RecylelistWorld);
        LinearLayoutManager llm = new LinearLayoutManager(Activity_World_List.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        RecycleListWorlds.setLayoutManager(llm);
        wra = new World_RecyclerAdapter(Activity_World_List.this, db.WorldQuery(SQLite.ALL_COLUMNS_WORLD, null, null, null, null, null), AppConfig.DefaultWorldView);
        RecycleListWorlds.setAdapter(wra);
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    deleteWorld(String.valueOf(viewHolder.itemView.getTag()));
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("ID",String.valueOf(viewHolder.itemView.getTag()));
                    bundle.putString("Type","Edit");
                    tm.IntentData(getApplicationContext(),Activity_Add_World.class,bundle);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"));
                        //background colour
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop() + 2, dX, (float) itemView.getBottom() - 2);
                        c.drawRect(background, p);
                        float left = (float) itemView.getLeft() + width;
                        float top = (float) itemView.getTop() + width;
                        float right = (float) itemView.getLeft() + 2 * width;
                        float bottom = (float) itemView.getBottom() - width;
                        icon = getBitmapFromVectorDrawable(getApplicationContext(), R.drawable.edit);
                        RectF iconDest = new RectF(left, top, right, bottom);
                        c.drawBitmap(icon, null, iconDest, p);
                    } else if (dX < 0) {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop() + 2, (float) itemView.getRight(), (float) itemView.getBottom() - 2);
                        c.drawRect(background, p);
                        icon = getBitmapFromVectorDrawable(getApplicationContext(), R.drawable.delete);
                        float left = (float) itemView.getRight() - 2 * width;
                        float top = (float) itemView.getTop() + width;
                        float right = (float) itemView.getRight() - width;
                        float bottom = (float) itemView.getBottom() - width;
                        RectF iconDest = new RectF(left, top, right, bottom);
                        c.drawBitmap(icon, null, iconDest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(RecycleListWorlds);
        //endregion
    }
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem search = menu.findItem(R.id.menuSearch);
        final MenuItem add = menu.findItem(R.id.add);
        MenuItem desc = menu.findItem(R.id.desc);
        MenuItem asc = menu.findItem(R.id.asc);
        MenuItem detail = menu.findItem(R.id.detailed);
        MenuItem normal = menu.findItem(R.id.normal);
        detail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                wra.setViewType(1);
                LoadAllWorlds("", "", listViewLimit);
                return false;
            }
        });
        normal.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                wra.setViewType(0);
                LoadAllWorlds("", "", listViewLimit);
                return false;
            }
        });

        desc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllWorlds("", " DESC ", listViewLimit);
                return false;
            }
        });
        asc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                LoadAllWorlds("", " ASC ", listViewLimit);
                return false;
            }
        });



        add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final User user = db.GetUserById("1");
                if (RecycleListWorlds.getAdapter().getItemCount() > 0) {
                    if (config.ShowAds) {
                        if (RecycleListWorlds.getAdapter().getItemCount() >= Integer.valueOf(db.GetUserById(user.getUserID()).getUserWorldCount())) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_World_List.this);
                            alertDialogBuilder.setTitle("You have " + String.valueOf(RecycleListWorlds.getAdapter().getItemCount()) + "/" + db.GetUserById(user.getUserID()).getUserWorldCount() + ", Worlds");
                            alertDialogBuilder
                                    .setMessage("Do you want to watch an Ad for 5 more worlds?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            LoadWorldAds la = new LoadWorldAds();
                                            la.execute();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } else {
                            tm.intent(getApplicationContext(), Activity_Add_World.class);
                        }
                    }
                    return false;
                } else{
                    tm.intent(getApplicationContext(), Activity_Add_World.class);
                }
                return false;
            }

        });
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                LoadAllWorlds(newText, "", listViewLimit);

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    private void deleteWorld(String ID) {
        db.DeleteWorld(ID);
        LoadAllWorlds("", "", listViewLimit);
    }
    public void LoadAllWorlds(String name, String order, String Limit) {
        new LoadWorlds().execute(name, order, Limit);
    }
    private class LoadWorlds extends AsyncTask<String, Void, Void> {
        Cursor c;
        private ProgressDialog dialog = new ProgressDialog(Activity_World_List.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                String Like = "";
                String Order = "";
                String Limit = "";
                if (params[0] != null) {
                    Like = params[0];
                }
                if (params[1] != null) {
                    Order = params[1];
                }
                if (params[2] != null) {
                    Limit = params[2];
                }
                 c = db.WorldQuery(SQLite.ALL_COLUMNS_WORLD, SQLite.WORLD_NAME + " LIKE '%" + Like + "%'", null, null, SQLite.WORLD_NAME + " " + Order + " LIMIT " + Limit, null);


            } catch (Exception x) {
                Log.e("Errr", x.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            wra.swapCursor(c);

        }
    }
    private class LoadWorldAds extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog = new ProgressDialog(Activity_World_List.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }


        @Override
        protected Void doInBackground(String... params) {


            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            adMob.showAdMobRewardVideo();
        }
    }
}

