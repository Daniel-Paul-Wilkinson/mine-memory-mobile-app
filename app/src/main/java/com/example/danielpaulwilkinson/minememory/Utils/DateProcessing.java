package com.example.danielpaulwilkinson.minememory.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateProcessing {

    //region VARIABLES
    //CALENDAR INSTANCE
    private Calendar c = Calendar.getInstance();
    //endregion

    //region DATE FORMATS
    private SimpleDateFormat myDisplayDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public SimpleDateFormat mySQLDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    //endregion

    //region DAY METHODS
    public String getWeekStartDay() {
        Calendar calendar = (Calendar) c.clone();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return myDisplayDateFormat.format(calendar.getTime());
    }

    public String getWeekEndDay() {
        Calendar calendar = (Calendar) c.clone();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        return String.valueOf(myDisplayDateFormat.format(calendar.getTime()));
    }

    public String returnDate(Date today) {
        return myDisplayDateFormat.format(today);
    }
    //endregion

    //region WEEK METHODS
    public List<String> getDatesThisWeek() {
        Calendar calendar = (Calendar) c.clone();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        List<String> Dates = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            Date dt = calendar.getTime();
            // now format it using SimpleDateFormat
            String val = myDisplayDateFormat.format(dt);
            Dates.add(val);
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        return Dates;
    }
    //endregion

    //region MONTH METHODS
    public List<String> getAllMonthDates() {
        Calendar cal = (Calendar) c.clone();
        setTimeToBeginningOfDay(c);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int myMonth = cal.get(Calendar.MONTH);
        List<String> dates = new ArrayList<>();
        while (myMonth == cal.get(Calendar.MONTH)) {
            dates.add(myDisplayDateFormat.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return dates;
    }

    public String getMonthStartDate() {
        Calendar calendar = (Calendar) c.clone();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setTimeToBeginningOfDay(calendar);
        return myDisplayDateFormat.format(calendar.getTime());
    }

    public String getMonthEndDate() {
        Calendar calendar = (Calendar) c.clone();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndofDay(calendar);
        return myDisplayDateFormat.format(calendar.getTime());
    }

    public int getMonthDayNumber() {
        Calendar calendar = (Calendar) c.clone();
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public int getCurrentMonthNumber() {
        Calendar cal = (Calendar) c.clone();
        return cal.get(Calendar.MONTH) + 1;
    }

    private int getDaysInCurrentMonth(int Month) {

        int daysinMonth = 0;

        switch (Month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                daysinMonth = 31;
                break;
            case 2:
                daysinMonth = 28;
            case 4:
            case 6:
            case 9:
            case 11:
                daysinMonth = 30;
        }
        return daysinMonth;
    }
    //endregion

    //region YEAR METHODS
    //endregion

    //region CALENDAR METHODS
    public String getYesterdayDate(int minus) {
        Calendar yesterday = (Calendar) c.clone();
        yesterday.add(Calendar.DATE, -minus);
        return myDisplayDateFormat.format(yesterday.getTime());
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public void setCalenderIntance(Date date) {
        c.setTime(date);
    }
    //endregion

    //region TIME METHODS
    public String calculateTimeSincePost(Date today, Date posted) {


        long diff = today.getTime() - posted.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (60 * 60 * 1000 * 24);//Look into this....
        long diffWeeks = diff / (60 * 60 * 1000 * 24 * 7);
        long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
        long diffYears = (long) (diff / (1000 * 60 * 60 * 24 * 365));
        //TODO: make this a switch statement
        if (diffSeconds < 1) {
            return "one sec ago";
        } else if (diffMinutes < 1) {
            if (diffSeconds == 1) {
                return "Created " + diffSeconds + " second ago";
            } else {
                return "Created " + diffSeconds + " seconds ago";
            }
        } else if (diffHours < 1) {
            if (diffMinutes == 1) {
                return "Created " + diffMinutes + " minute ago";
            } else {
                return "Created " + diffMinutes + " minutes ago";
            }
        } else if (diffDays < 1) {
            if (diffHours == 1) {
                return "Created " + diffHours + " hour ago";
            } else {
                return "Created " + diffHours + " hours ago";
            }
        } else if (diffWeeks < 1) {
            if (diffDays == 1) {
                return "Created " + diffDays + " day ago";
            } else {
                return "Created " + diffDays + " days ago";
            }
        } else if (diffMonths < 1) {
            if (diffWeeks == 1) {
                return "Created " + diffWeeks + " week ago";
            } else {
                return "Created " + diffWeeks + " weeks ago";
            }
        } else if (diffYears < 12) {
            if (diffMonths == 1) {
                return "Created " + diffMonths + " month ago";
            } else {
                return "Created " + diffMonths + " months ago";
            }
        } else {
            if (diffYears > 1) {
                return "Created" + diffYears + " years ago";

            } else {
                return "Created" + diffYears + " year ago";

            }
        }
    }
    //endregion

}



