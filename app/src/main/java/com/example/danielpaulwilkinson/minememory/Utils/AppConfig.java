package com.example.danielpaulwilkinson.minememory.Utils;

import com.example.danielpaulwilkinson.minememory.BuildConfig;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.Logger;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.LoggerList;
import java.util.ArrayList;

public class AppConfig {

    //region HOME-SETTINGS
    public boolean TutorialEnabled = false;
    public boolean AccountEnabled = false;
    public boolean WorldEnabled = true;
    public boolean CraftManualEnabled = false;
    //endregion

    //region IMAGE-SETTINGS
    public static int ImageListQuality = 1;
    public static int ImageDetailQuality = 2;
    //endregion

    //region AD-SETTINGS
    public static Boolean ShowAds = true;
    public static String setAdThreshold = "2";
    //endregion

    //region WORLD-SETTINGS
    public static ArrayList<String> returnWorldDifficulties(){
        ArrayList<String> difficulty = new ArrayList<>();
        difficulty.add("Easy");
        difficulty.add("Normal");
        difficulty.add("Hard");
        difficulty.add("Hardcore");
        difficulty.add("Creative");
        return  difficulty;
    }
    //endregion

    //region DEFAULT-RECYCLE-VIEWS
    /**
     * World Recycler default view setting
     * 0 - Normal view
     * 1 - Detail View
     * User may want to set this?
     * */
    public static int DefaultWorldView = 0;

    //endregion

    //region CONFIG-KEYS
    public static boolean InDebug(){
        return BuildConfig.DEBUG;
    }
    public static String returnAdmobKey() {
        if (InDebug()) {
            LoggerList.addLogItem(new Logger("DEBUG MODE","APP IS USING TEST ADS"));
            return "ca-app-pub-3940256099942544/5224354917";
        } else {
            return "ca-app-pub-9605974313059945/3439862810";
        }
    }
    //endregion
}

