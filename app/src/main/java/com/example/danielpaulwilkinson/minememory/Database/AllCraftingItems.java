package com.example.danielpaulwilkinson.minememory.Database;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.danielpaulwilkinson.minememory.Object_Classes.CraftingItem;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;

import java.util.ArrayList;

/**
 * Created by DanielPaulWilkinson on 06/06/2017.
 */

public class AllCraftingItems {

    private Context contexts;
    private ArrayList<CraftingItem> list = new ArrayList<>();

    public AllCraftingItems(Context context) {
        contexts = context;
    }

    public ArrayList<CraftingItem> ReturnList() {

        ImageProcessing im = new ImageProcessing(contexts);

        list.add(new CraftingItem("", "", "Button", "A button is a non-solid face00 which can provide temporary redstone power.", "2.5", "Yes (64)", "No", "No", "Itself", "10/10/10", "Mechanisms"));
        list.add(new CraftingItem("", "", "Dropper", "The dropper is a face00 that can be used to eject items, or push items into another container.", "17.5", "Yes (64)", "No", "No", "Itself and stuff inside", "10/10/10", "Mechanisms"));
        list.add(new CraftingItem("", "", "Daylight Sensor", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Door", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Dispenser", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Gate", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Dropper", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Switch", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Note Block", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Observer", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Piston", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Pressure Plate", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Rails", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Redstone Comparator", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Redstone Lamp", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Redstone Repeater", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Trap Door", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Tripwire Hook", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", "", "Redstone Repeater", "", "", "", "", "", "", "", "Mechanisms"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.shovel)), "Shovel", "Shovels are tools used to ease the process of collecting dirt and other blocks.", "60-1562", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.sheers)), "Shears", "Shears are a tool used primarily to shear sheep and mine a few types of blocks.", "238", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.hoe)), "Hoe", "Hoes are tools used to till dirt and grass blocks into farmland blocks.", "60-1562", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.rod)), "Fishing Rod", "Fishing rods are tools whose primary purpose is obtaining fish.", "65", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.axe)), "Axe", "Axes are tools used to ease the process of collecting wood based items.", "6-1562", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.clock)), "Clock", "Clocks are items that display the current in-game time by displaying the sun and the moon's position relative to the horizon.", "N/A", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.compass)), "Compass", "Compasses are an item used to point to the world spawn.", "N/A", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.lead)), "Lead", "Leads, also known as leashes, are items used to leash and lead passive mobs.", "N/A", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.map)), "Map", "A map is an item used to view explored terrain.", "N/A", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.bow)), "Bow", "A bow is a ranged weapon that fires arrows.", "385", "No", "No", "No", "Itself", "10/10/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.flintandsteel)), "Flint and Steal", "The flint and steel is a tool used to light fires.", "65", "No", "Yes", "No", "Itself", "10/11/10", "Tool"));
        list.add(new CraftingItem("", im.SaveImageToInternalStorage("craftimages", BitmapFactory.decodeResource(contexts.getResources(), R.drawable.pickaxe)), "Pickaxe", "Pickaxes are one of the most commonly used tools in the game, being required to mine all ores and many other types of blocks.", "60-1562", "No", "Yes", "No", "Itself", "11/10/10", "Tool"));
        list.add(new CraftingItem("", "", "Anvil", "Anvils are blocks that use the item repair interface to repair and to rename items, as well as to combine enchantments.", "6,000", "No", "No", "No", "Itself", "11/10/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Beacon", "A beacon is a unique face00 that projects a light beam skyward and can provide status effects to players in the vicinity.", "15", "Yes (64)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Bed", "A bed is a face00 that allows a player to sleep and to reset their spawn point to within a few blocks of the bed.", "1", "No", "Lava", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Brewing Stand", "A brewing stand is a face00 used for brewing potions, splash potions and lingering potions.", "2.5", "Yes (65)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Cauldron", "A cauldron is a face00 that can hold water.", "10", "Yes (64)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Chest", "A chest is a face00 that stores items.", "12.5", "Yes (64)", "No, Catches fire from lava.", "No", "Itself & everything contained", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Crafting_Adapter Bench", "The crafting table (previously known as the workbench) is one of the most essential blocks in Minecraft.", "12.5", "Yes (64)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Enchantment Table", "An enchantment table is a face00 that allows players to spend their experience point levels to enchant tools, books and armor.", "6,000", "Yes (64)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Farmland", "Farmland is a face00 on which seeds can be planted and grown.", "3", "Yes (64)", "No", "No", "Dirt", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Frosted Ice", "Frosted ice is a translucent solid face00.", "2.5", "No", "No", "No", "Nothing", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Furnace", "A furnace is a face00 used to smelt blocks and items and convert them into other blocks or items.", "17.5", "Yes (64)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Skulker Box", "A shulker box is a face00 that can store and transport items.", "30", "No", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Skulls", "Mob heads are decorative blocks.", "5", "Yes (65)", "No", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Sign", "Signs are non-solid blocks that can display text.", "5", "Yes (64)", "Yes, in pocket edition.", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "TNT", "TNT is a face00 that can be used by the player to initiate a controlled explosion.", "0", "Yes (64)", "Yes", "No", "Itself", "10/11/10", "ColourProcessing"));
        list.add(new CraftingItem("", "", "Torch", "Torches are non-solid blocks that emit light.", "0", "Yes (64)", "Yes", "No", "Itself", "10/11/10", "ColourProcessing"));

        return list;
    }


}
