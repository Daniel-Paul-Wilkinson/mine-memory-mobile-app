package com.example.danielpaulwilkinson.minememory.Activity_Add;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_Item_List;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.DateProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.FormValidation;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;
import com.example.danielpaulwilkinson.minememory.Object_Classes.WorldItem;
import com.example.danielpaulwilkinson.minememory.R;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

public class Activity_Add_Item extends BaseActivity {

    //region VARIABLES
    EditText name ,note, X, Y, Z;
    Switch loot;
    Button btnAddItem, btnUploadImage;
    ImageView imgWorldItem;
    DateProcessing date;
    SQLite db;
    ImageProcessing is;
    IntentManager tm;
    String WorldID, SwitchChecked;
    Animation an;
    Async_InsertWorldItem async_insertWorldItem;

    private final int REQUEST_CODE_GET_CAMERA_IMAGE = 4, REQUEST_CODE_GET_GALLERY_IMAGE = 5, REQUEST_CODE_SELECTED_IMAGE = 6;
    //endregion

    //region ONCREATE
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.activity_add_item);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         db = new SQLite(getApplicationContext());
         date = new DateProcessing();
         is = new ImageProcessing(getApplicationContext());
         tm = new IntentManager();
         async_insertWorldItem = new Async_InsertWorldItem();
         Intent intent = getIntent();
         Bundle extras = intent.getExtras();
         assert extras != null;
         WorldID = extras.getString("IDs");



        setTitle("Add A World Item");

        an = new AlphaAnimation(0.5f,1f);
        an.setDuration(2000);


        name = (EditText) findViewById(R.id.txtItemName);
        note = (EditText) findViewById(R.id.txtItemNote);
        loot = (Switch) findViewById(R.id.swLoot);
        X = (EditText) findViewById(R.id.txtItemX);
        Y = (EditText) findViewById(R.id.txtItemY);
        Z = (EditText) findViewById(R.id.txtItemZ);
        btnAddItem = (Button) findViewById(R.id.btnAddItem);
        btnUploadImage = (Button) findViewById(R.id.btnUploadItem);
        imgWorldItem = (ImageView) findViewById(R.id.imgWorldItem);
        imgWorldItem.setImageBitmap(is.returnRandomMinecraftImage());

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnUploadImage.startAnimation(an);
                CameraModel();
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnAddItem.startAnimation(an);
                AddNewWorldItem();
            }
        });
        
        loot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SwitchChecked = String.valueOf(isChecked);
                if(isChecked){
                    buttonView.setText("Yes");
                } else{
                    buttonView.setText("No");
                }
            }
        });
    }
    //endregion

    //region ADD-NEW-WORLD
    private void AddNewWorldItem() {
        Boolean XisValid = FormValidation.basicEmptyStringValidation(X,"Please enter a X-Axis value.");
        Boolean YisValid = FormValidation.basicEmptyStringValidation(Y,"Please enter a Y-Axis value.");
        Boolean ZisValid = FormValidation.basicEmptyStringValidation(Z,"Please enter a Z-Axis value.");
        Boolean NameIsValid = FormValidation.basicEmptyStringValidation(name,"Please enter a Name.");
        if(NameIsValid && XisValid && ZisValid && YisValid){
            Bitmap bm = ((BitmapDrawable) imgWorldItem.getDrawable()).getBitmap();
            async_insertWorldItem.execute(new WorldItem(
                    X.getText().toString().trim(),
                    Y.getText().toString().trim(),
                    Z.getText().toString().trim(),
                    String.valueOf(SwitchChecked),
                    name.getText().toString().trim(),
                    note.getText().toString().trim(),
                    WorldID,
                    date.mySQLDateFormat.format(new Date()),
                    is.SaveImageToInternalStorage("Mine_Memory",bm)));
        }
    }
    //endregion

    //region SELECT-IMAGE
    private void CameraModel() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_Add_Item.this);
        alertDialogBuilder.setTitle("Image Selection");
        alertDialogBuilder
                .setMessage("Where would you like to get an image from?")
                .setCancelable(false)
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {startCamera();}})
                .setNegativeButton("Libary",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {startGallery();}})
                .setNeutralButton("Return", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id){}});
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_GET_CAMERA_IMAGE);
    }
    private void startGallery() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_CODE_GET_GALLERY_IMAGE);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_SELECTED_IMAGE);
            }
        }
    }
    //endregion-------------

    //region PERMISSIONS-AND-ACTIVITY-RESULT
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                    super.onActivityResult(requestCode,resultCode,data);
                    switch (requestCode) {
                        case REQUEST_CODE_GET_GALLERY_IMAGE:
                            try {
                                if (resultCode == Activity.RESULT_OK) {
                                    Uri selectedImage = data.getData();
                                    InputStream imageStream = null;
                                    imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImage);
                                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                                    imgWorldItem.setImageBitmap(yourSelectedImage);
                                }
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            break;
                        case REQUEST_CODE_GET_CAMERA_IMAGE:
                            if (resultCode == Activity.RESULT_OK) {
                                Bitmap photo = (Bitmap) data.getExtras().get("data");


                                imgWorldItem.setImageBitmap(photo);
                            }
                            break;
                        default:
                            break;
                    }
                }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_SELECTED_IMAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startGallery();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "This feature requires an image from your storage", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    //endregion

    //region ASYNC-ADD-WORLD-ITEM
    @SuppressLint("StaticFieldLeak")
        private class Async_InsertWorldItem extends AsyncTask<WorldItem, Void, Void> {
        @Override
        protected Void doInBackground(WorldItem... worldItems) {
            db.InsertWorldItem(worldItems[0]);

            return null;
        }
        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            Toast.makeText(Activity_Add_Item.this,"Location Added",Toast.LENGTH_SHORT).show();
            tm.intent(Activity_Add_Item.this,Activity_Item_List.class);
        }
    }
    //endregion

}