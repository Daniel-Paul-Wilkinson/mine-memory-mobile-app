package com.example.danielpaulwilkinson.minememory.Adapters.World;

/**
 * Created by DanielPaulWilkinson on 30/05/2017.
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_Item_List;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.R;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ColourProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.DateProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;

import java.text.ParseException;
import java.util.Date;

//look into this https://gist.github.com/afollestad/c84e259e98a03134dfd5

public class World_RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Cursor c;
    private ImageProcessing ia;
    private SQLite db;
    private AppConfig config;
    private IntentManager tm;
    private DateProcessing dateProcessing;
    private int viewType;


    public World_RecyclerAdapter(Context context, Cursor c, int viewType) {
        this.context = context;
        this.c = c;
        this.viewType = viewType;
        ia = new ImageProcessing(context);
        db = new SQLite(context);
        config = new AppConfig();
        tm = new IntentManager();
        dateProcessing = new DateProcessing();
    }


    class WorldViewHolderSmall extends RecyclerView.ViewHolder {
        TextView nameTextView;
        ImageView path;
        RelativeLayout normal;
        TextView txtTimeSinceWorld;
        WorldViewHolderSmall(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.txtWorldName);
            path = (ImageView) itemView.findViewById(R.id.imgWorldName);
            normal = (RelativeLayout) itemView.findViewById(R.id.WorldSmallLayout);
            txtTimeSinceWorld = (TextView) itemView.findViewById(R.id.txtTimeSinceWorld);
        }
    }
    class WorldViewHolderLarge extends RecyclerView.ViewHolder {
        TextView txtxlworldname;
        ImageView imgWorldName;
        TextView txtxlDesc;
        TextView txtxlDiff;
        TextView txtWorldItemCount;
        WorldViewHolderLarge(View itemView) {
            super(itemView);
            txtxlworldname = (TextView) itemView.findViewById(R.id.txtxlworldname);
            imgWorldName = (ImageView) itemView.findViewById(R.id.imgWorldName);
            txtxlDesc = (TextView) itemView.findViewById(R.id.txtxlDesc);
            txtxlDiff = (TextView) itemView.findViewById(R.id.txtxlDiff);
            txtWorldItemCount = (TextView) itemView.findViewById(R.id.txtWorldItemCount);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            return new WorldViewHolderSmall(LayoutInflater.from(context).inflate(
                    R.layout.world_listitem_small, parent, false));
        }else if(viewType == 1){
            return new WorldViewHolderLarge(LayoutInflater.from(context).inflate(
                    R.layout.world_listitem_xlarge, parent, false));
        }else{
            return null;
        }

    }



    public void setViewType(int selectedviewType){
       viewType =  selectedviewType;
    }


    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!c.moveToPosition(position)) {
            return;
        }
        switch (holder.getItemViewType()) {
            case 0: {
                WorldViewHolderSmall view = (WorldViewHolderSmall)holder;
                view.nameTextView.setText(c.getString(c.getColumnIndex(SQLite.WORLD_NAME)));
                view.path.setImageBitmap(ia.loadImage(c.getString(c.getColumnIndex(SQLite.WORLD_IMAG)), config.ImageListQuality));
                view.itemView.setTag(c.getString(c.getColumnIndex(SQLite.WORLD_ID)));
                try {
                view.txtTimeSinceWorld.setText(dateProcessing.calculateTimeSincePost(new Date(),
                        dateProcessing.mySQLDateFormat.parse(c.getString(c.getColumnIndex(SQLite.WORLD_DATE)))));
                } catch (ParseException e) {
                    view.txtTimeSinceWorld.setText(dateProcessing.mySQLDateFormat.format(new Date()));
                }
                view.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bn = new Bundle();
                        bn.putString("ID", (String) view.getTag());
                        tm.IntentData(context, Activity_Item_List.class, bn);
                    }
                });
            }





            break;
            case 1:{
                WorldViewHolderLarge view = (WorldViewHolderLarge) holder;
                view.txtxlworldname.setText(c.getString(c.getColumnIndex(SQLite.WORLD_NAME)));
                view.imgWorldName.setImageBitmap(ia.loadImage(c.getString(c.getColumnIndex(SQLite.WORLD_IMAG)), config.ImageListQuality));
                view.itemView.setTag(c.getString(c.getColumnIndex(SQLite.WORLD_ID)));
                view.txtxlDesc.setText(c.getString(c.getColumnIndex(SQLite.WORLD_DESC)));
                view.txtxlDiff.setText(c.getString(c.getColumnIndex(SQLite.WORLD_DIFF)));
                view.txtWorldItemCount.setText(db.GetWorldChildCount(SQLite.TABLE_ITEM,c.getString(c.getColumnIndex(SQLite.WORLD_ID))));
                view.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bn = new Bundle();
                        bn.putString("ID", (String) view.getTag());
                        tm.IntentData(context, Activity_Item_List.class, bn);
                    }
                });

                view.txtxlDiff.setTextColor(Color.parseColor(ColourProcessing.returnDiffColour(view.txtxlDiff.getText().toString())));






            }
                break;
        }
    }


    public int getItemCount () {
            return c.getCount();
    }
    public void swapCursor(Cursor newCursor) {
        if (c != null) {
            c.close();
        }
        c = newCursor;

        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }
}
