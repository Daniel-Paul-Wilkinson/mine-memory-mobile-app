package com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import java.util.ArrayList;

public class ImageProcessing_Lists {

    //region VARIABLES
    private Context con;

    /**
     * Used in "ImageProcessing" as a placeholder array to ensure we get a new rand
     * every time. This means we can remove an array number once we have used it and
     * add numbers if the count of ints in this array is 0 returning a new number
     * every time.
     *
     * Moved here as if we had this in "ImageProcessing" we would be reinitializing
     * this array every time.
     * */
    public final static ArrayList<Integer> DummyIntArray = new ArrayList<Integer>();
    //endregion

    //region CONSTRUCTOR
    public ImageProcessing_Lists(Context con) {
        this.con = con;
    }
    //endregion

}