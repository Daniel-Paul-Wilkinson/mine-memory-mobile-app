package com.example.danielpaulwilkinson.minememory.Adapters.World;

/**
 * Created by DanielPaulWilkinson on 30/05/2017. -- no longer used as is inefficent
 */
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing.ImageProcessing;
import com.example.danielpaulwilkinson.minememory.R;

public class World_Adapter extends CursorAdapter{

        private ImageProcessing ia;
        private SQLite db;
        private AppConfig config;

        public World_Adapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            ia = new ImageProcessing(context);
            db = new SQLite(context);
            config = new AppConfig();

        }
        @Override
        //infiltrate the layout
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return LayoutInflater.from(context).inflate(
                    R.layout.world_listitem_small,viewGroup,false );
        }
        @Override
        //get the correct view and apply the data to the text fields
        public void bindView(View view, Context context, Cursor cursor) {
            TextView nameTextView = (TextView) view.findViewById(R.id.txtxlworldname);
            ImageView path = (ImageView) view.findViewById(R.id.imgWorldName);
            nameTextView.setText(cursor.getString(cursor.getColumnIndex(SQLite.WORLD_NAME)));
            path.setImageBitmap(ia.loadImage(cursor.getString(cursor.getColumnIndex(SQLite.WORLD_IMAG)),config.ImageListQuality));
    }
}
