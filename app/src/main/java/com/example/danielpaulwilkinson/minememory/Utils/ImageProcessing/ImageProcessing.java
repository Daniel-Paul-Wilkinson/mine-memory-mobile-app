package com.example.danielpaulwilkinson.minememory.Utils.ImageProcessing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Log;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.R;

import static android.graphics.BitmapFactory.decodeFile;

public class ImageProcessing {

    //region VARIABLES
    private SQLite db;
    private Context contexts;
    //endregion

    //region CONSTRUCTOR
    public ImageProcessing(Context context){
        contexts = context;
    }
    //endregion

    //region IMAGE-CRUID-METHODS
    public String SaveImageToInternalStorage(String name, Bitmap bitmapImage){
        File MyPath = new File(generateName(name,true));
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(MyPath);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 50, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert fos != null;
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return MyPath.toString();
    }
    /**
     * Returns either the full path to the file or the file name based upon bool. Found basis here:
     * https://stackoverflow.com/questions/26570084/how-to-get-file-name-from-file-path-in-android
     * */
    public String generateName(String CustomImageName, Boolean getFullPath){
        String path;
        ContextWrapper cw = new ContextWrapper(contexts.getApplicationContext());
        File directory = cw.getDir("minememory", Context.MODE_PRIVATE);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyymmsshhmmss");
        String date = sdf.format(new Date());
        path = directory + CustomImageName +"_"+ date + ".jpg";
        if(getFullPath) {
            return path;

        } else{
            return path.substring(path.lastIndexOf("/")+1);
        }
    }
    /**
     *Gets an image from private storage
     * @param path - Will return the bitmap from the path
     * @param ImageSize - Will return the bitmap quality depending upon value.
     * */
    public Bitmap loadImage(String path, int ImageSize) {
        try {
            return decodeFile(String.valueOf(new File(path)),LoadBitmapOptions(ImageSize));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }
    /**
     *Gets an image from private storage
     * @param path - Will delete image from the path of an image.
     * */
    public Boolean deleteImage(String path) {
        Boolean d;
        try {
            File f = new File(path);
            d = f.getCanonicalFile().delete();
            if(!new File(path).exists()){
                Log.e("Deleted: ",path);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return d;
    }
    //endregion

    //region IMAGE-PROCESSING
    public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    //endregion

    //region APP-RELATED-IMAGE-PROCESSING
    /**
     * returns bitmap image if user doesn't want to upload a photo.
     * used in Activity_Add Folder.
     */
    public Bitmap returnRandomMinecraftImage(){

        ArrayList<Bitmap> drawables = new ArrayList<>();
        drawables.add(BitmapFactory.decodeResource(contexts.getResources(),R.drawable.face00));
        drawables.add(BitmapFactory.decodeResource(contexts.getResources(),R.drawable.face01));
        drawables.add(BitmapFactory.decodeResource(contexts.getResources(),R.drawable.face02));
        drawables.add(BitmapFactory.decodeResource(contexts.getResources(),R.drawable.face03));

        if (ImageProcessing_Lists.DummyIntArray.size() <= 0) {
            for (int c = 0; c < drawables.size(); ++c) {
                ImageProcessing_Lists.DummyIntArray.add(c);
            }
        }

        int arrIndex = (int)((double)ImageProcessing_Lists.DummyIntArray.size() * Math.random());
        int randomIndex = ImageProcessing_Lists.DummyIntArray.get(arrIndex);
        ImageProcessing_Lists.DummyIntArray.remove(arrIndex);
        return drawables.get(randomIndex);
    }
    /**
     *  This method is used when we read an image in from the gallery so we can
     *  show the user that we have their selected image via the image view and
     *  the textview. This only works via gallery selection.
     * */
    public static String getFileName(Uri uri, Context c) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    //endregion

    //region HANDLING-MEMORY
    public BitmapFactory.Options LoadBitmapOptions(int BitmapSize){
        BitmapFactory.Options options = new BitmapFactory.Options();

        switch(BitmapSize){
            //the size used in a list view for worlds and world items.
            case 1:
                options.inScaled = true;
                options.inSampleSize = 8;
                options.outWidth = 10;
                options.outHeight = 10;
                break;
            //the size used for detail views and large images which require more memory to load.
            case 2:
                //nothing atm need to do more research into this...
                break;
        }


        return options;
    }



//    public void SavingBitmapIntoMemory(){
//        Bitmap bitmapOne = BitmapFactory.decodeFile(filePathOne);
//        imageView.setImageBitmap(bitmapOne);
//        // lets say , we do not need image bitmapOne now and we have to set // another bitmap in imageView
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(filePathTwo, options);
//
//        if (canUseForInBitmap(bitmapOne, options)) {
//    //canUseForInBitmap check if the image can be reuse or not by //checking the image size and other factors
//            options.inMutable = true;
//            options.inBitmap = bitmapOne;
//        }
//        options.inJustDecodeBounds = false;
//        Bitmap bitmapTwo = BitmapFactory.decodeFile(filePathTwo, options);
//        imageView.setImageBitmap(bitmapTwo);
//    }
//    public static boolean canUseForInBitmap(
//            Bitmap candidate, BitmapFactory.Options targetOptions) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            // From Android 4.4 (KitKat) onward we can re-use if the byte size of
//            // the new bitmap is smaller than the reusable bitmap candidate
//            // allocation byte count.
//            int width = targetOptions.outWidth / targetOptions.inSampleSize;
//            int height = targetOptions.outHeight / targetOptions.inSampleSize;
//            int byteCount = width * height * getBytesPerPixel(candidate.getConfig());
//
//            try {
//                return byteCount <= candidate.getAllocationByteCount();
//            } catch (NullPointerException e) {
//                return byteCount <= candidate.getHeight() * candidate.getRowBytes();
//            }
//        }
//// On earlier versions, the dimensions must match exactly and the inSampleSize must be 1
//        return candidate.getWidth() == targetOptions.outWidth
//                && candidate.getHeight() == targetOptions.outHeight
//                && targetOptions.inSampleSize == 1;
//    }
//
//
//    private static int getBytesPerPixel(Bitmap.Config config) {
//        if (config == null) {
//            config = Bitmap.Config.ARGB_8888;
//        }
//        int bytesPerPixel;
//        switch (config) {
//            case ALPHA_8:
//                bytesPerPixel = 1;
//                break;
//            case RGB_565:
//            case ARGB_4444:
//                bytesPerPixel = 2;
//                break;
//            case ARGB_8888:
//            default:
//                bytesPerPixel = 4;
//                break;
//        }
//        return bytesPerPixel;
//    }
    //endregion

}
