package com.example.danielpaulwilkinson.minememory.Google;

import android.content.Context;
import android.widget.Toast;

import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.Logger;
import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.LoggerList;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AndroidGoogleAdMob implements RewardedVideoAdListener {

    private static RewardedVideoAd rewardedVideoAdAddWorlds;
    private Context context;
    private SQLite db;

    public AndroidGoogleAdMob(Context context, SQLite sql){
        this.context = context;
        this.db = sql;
    }

    public void initAdMob(){
        MobileAds.initialize(context,"ca-app-pub-9605974313059945~4163059181");
        rewardedVideoAdAddWorlds = MobileAds.getRewardedVideoAdInstance(context);
        rewardedVideoAdAddWorlds.setRewardedVideoAdListener(this);
    }

    public AdRequest makeRequest(Boolean isDebug){
        if (isDebug) {
            LoggerList.addLogItem(new Logger("DEBUG MODE","MAKE FRESH AD REQUEST"));
            return new AdRequest
                    .Builder()
                    .addTestDevice("BAAC69583FAA471534C0092550DFD10E")
                    .build();
        } else {
            return new AdRequest
                    .Builder()
                    .build();
        }
    }

    public boolean adIsLoaded(){
        return rewardedVideoAdAddWorlds.isLoaded();
    }

    public void loadAdMobRewardVideo(AdRequest adRequest, String code){
        rewardedVideoAdAddWorlds.loadAd(code,adRequest);
    }
    public Boolean showAdMobRewardVideo(){
        if(rewardedVideoAdAddWorlds.isLoaded()) {
            rewardedVideoAdAddWorlds.show();
            return true;
        }
        return false;
    }
    @Override
    public void onRewardedVideoAdLoaded() {
        //when ad loads successfully
    }

    @Override
    public void onRewardedVideoAdOpened() {
        //when ad opens and covers the screen
    }

    @Override
    public void onRewardedVideoStarted() {
        //when playback starts
    }

    @Override
    public void onRewardedVideoAdClosed() {
        //when the video ad is closed and we return
        this.loadAdMobRewardVideo(this.makeRequest(AppConfig.InDebug()), AppConfig.returnAdmobKey());
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        //when the user has watched the ad
        Toast.makeText(context,"Thanks fot your support! You earned +5 Worlds",Toast.LENGTH_LONG).show();
        int worldThresholdUpdate = 5 + Integer.valueOf(db.GetUserById("1").getUserWorldCount());
        LoggerList.addLogItem(new Logger("onReward","User Had: "+db.GetUserById("1").getUserWorldCount()+" User Has: "+String.valueOf(worldThresholdUpdate)));
        db.updateWorldCountById(1,worldThresholdUpdate);
    }
    @Override
    public void onRewardedVideoAdLeftApplication() {
        //to play store or destination URL
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        //load add failed
        this.loadAdMobRewardVideo(this.makeRequest(AppConfig.InDebug()), AppConfig.returnAdmobKey());
        LoggerList.addLogItem(new Logger("onAdFailedToLoad","Oops, something went wrong."));
    }

    @Override
    public void onRewardedVideoCompleted() {

    }

}