package com.example.danielpaulwilkinson.minememory.Object_Classes.Logger;

import android.util.Log;

import com.example.danielpaulwilkinson.minememory.Object_Classes.Logger.Logger;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;

import java.util.ArrayList;

public class LoggerList {

    static ArrayList<Logger> logger = new ArrayList<>();

    public static void addLogItem(Logger Log){
        logger.add(Log);
    }

    public static void displayLogs(){
        if(AppConfig.InDebug()) {
            int count = 0;
            for (Logger entry : logger) {
                count++;
                Log.e(" ID: "+String.valueOf(count)+" - "+entry.loggerTitle, entry.loggerMessge);

            }
        }
    }
}
