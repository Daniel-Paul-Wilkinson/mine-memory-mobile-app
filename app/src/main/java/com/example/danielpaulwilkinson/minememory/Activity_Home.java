package com.example.danielpaulwilkinson.minememory;

import android.annotation.SuppressLint;
import android.support.v7.app.ActionBar;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_Crafting_List;
import com.example.danielpaulwilkinson.minememory.Activity_Lists.Activity_World_List;
import com.example.danielpaulwilkinson.minememory.Database.SQLite;
import com.example.danielpaulwilkinson.minememory.Database.SharedPreferences;
import com.example.danielpaulwilkinson.minememory.Google.AndroidGoogleAdMob;
import com.example.danielpaulwilkinson.minememory.Utils.AppConfig;
import com.example.danielpaulwilkinson.minememory.Utils.BaseActivity;
import com.example.danielpaulwilkinson.minememory.Utils.IntentManager;
import com.example.danielpaulwilkinson.minememory.Utils.UserProcessing;

import java.util.ArrayList;

    //TODO: randomize the default image for both worlds and world and items. (could use minecraft heads)
    //TODO: Add statistics for other home page additions. Ensure each home icon looks clickable find relevant icon.
    //TODO: look at creating setting page maybe in account. CMS content?

public class Activity_Home extends BaseActivity {

    private IntentManager tm;
    private SQLite db;
    private TextView worldCount;
    private SharedPreferences sp;
    AndroidGoogleAdMob adMob;
    AppConfig core;
    TextView Title;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_home);
        //SETUP UTIL CLASSES
        tm = new IntentManager();
        core = new AppConfig();
        db = new SQLite(getApplicationContext());
        adMob = new AndroidGoogleAdMob(getApplicationContext(), db);
        sp = new SharedPreferences(getApplicationContext());


        //SETUP TITLE
        View view = getLayoutInflater().inflate(R.layout.actitivty_home_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);

        Title = (TextView) view.findViewById(R.id.actionbar_title);

        getSupportActionBar().setCustomView(view, params);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        centerTitle();

        //WORLD LIST SETUP
        ImageView imgWorldList = (ImageView) findViewById(R.id.imgWorldHome);
        TextView txtWorldList = (TextView) findViewById(R.id.txtWorldHome);
        TextView txtWorldComingSoon = (TextView) findViewById(R.id.txtHomeComingSoon);
        worldCount = (TextView) findViewById(R.id.txtWorldCreated);
        SetupEnabledWorld(txtWorldList, txtWorldComingSoon, imgWorldList, core.WorldEnabled);

        //SETUP CRAFTING LIST
        ImageView imgCraftList = (ImageView) findViewById(R.id.imgCraftManual);
        TextView txtCraftList = (TextView) findViewById(R.id.txtCraftManual);
        TextView txtCraftComingSoon = (TextView) findViewById(R.id.txtCraftComingSoon);
        SetupEnabledCrafting(txtCraftList, txtCraftComingSoon, imgCraftList, core.CraftManualEnabled);

        //SETUP TUTORIAL LIST
        ImageView imgTutorialList = (ImageView) findViewById(R.id.imgTutorial);
        TextView txtTutorialList = (TextView) findViewById(R.id.txtTutorial);
        TextView txtTutorialComingSoon = (TextView) findViewById(R.id.txtTutorialComingSoon);
        SetupEnabledTutorial(txtTutorialList, txtTutorialComingSoon, imgTutorialList, core.TutorialEnabled);

        //SETUP ACCOUNT
        ImageView imgAccountList = (ImageView) findViewById(R.id.imgAccount);
        TextView txtAccountList = (TextView) findViewById(R.id.txtAccount);
        TextView txtAccountComingSoon = (TextView) findViewById(R.id.txtAccountComingSoon);
        SetupEnabledAccount(txtAccountList, txtAccountComingSoon, imgAccountList, core.AccountEnabled);
        //store reference to this to ensure this has loaded.

        adMob.initAdMob();

        if (!adMob.adIsLoaded()) {
            adMob.loadAdMobRewardVideo(adMob.makeRequest(AppConfig.InDebug()), AppConfig.returnAdmobKey());
        }

        UserProcessing.InitAccount(sp,db);
    }



    private void centerTitle() {
        ArrayList<View> textViews = new ArrayList<>();
        getWindow().getDecorView().findViewsWithText(textViews, getTitle(), View.FIND_VIEWS_WITH_TEXT);
        if(textViews.size() > 0) {
            AppCompatTextView appCompatTextView = null;
            if(textViews.size() == 1) {
                appCompatTextView = (AppCompatTextView) textViews.get(0);
            } else {
                for(View v : textViews) {
                    if(v.getParent() instanceof Toolbar) {
                        appCompatTextView = (AppCompatTextView) v;
                        break;
                    }
                }
            }
            if(appCompatTextView != null) {
                ViewGroup.LayoutParams params = appCompatTextView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                appCompatTextView.setLayoutParams(params);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    appCompatTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        }
    }
    //region SETUP-CONTENT-ENABLED
    @SuppressLint("SetTextI18n")
    private void SetupEnabledWorld(TextView Title, TextView comingSoon, ImageView ImgView, Boolean value) {
        if(value){
            Title.setEnabled(true);
            Title.setVisibility(View.VISIBLE);
            Title.setAlpha(1f);

            comingSoon.setEnabled(false);
            comingSoon.setAlpha(1f);
            comingSoon.setVisibility(View.INVISIBLE);

            worldCount.setVisibility(View.VISIBLE);
            worldCount.setText("Worlds - "+db.GetCount(SQLite.TABLE_WORLD)+" | "+ "Locations - "+db.GetCount(SQLite.TABLE_ITEM));
            worldCount.setAlpha(1f);
            worldCount.setTextSize(30f);

            ImgView.setAlpha(1f);
            ImgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
            Title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
        } else {
            //ensure all fields are enabled
            Title.setEnabled(false);
            Title.setVisibility(View.VISIBLE);

            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.VISIBLE);

            //hide statistic if the element is set to false.
            worldCount.setVisibility(View.INVISIBLE);
            worldCount.setText("Created - "+db.GetCount(SQLite.TABLE_WORLD));
            worldCount.setAlpha(0f);

            Title.setOnClickListener(null);
            ImgView.setOnClickListener(null);

            ImgView.setAlpha(.2f);
            Title.setAlpha(.2f);
        }
    }
    private void SetupEnabledCrafting(TextView Title, TextView comingSoon, ImageView ImgView, Boolean value) {
        if(value){
            Title.setEnabled(true);
            Title.setVisibility(View.VISIBLE);
            Title.setAlpha(1f);
            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.INVISIBLE);
            comingSoon.setAlpha(1f);
            ImgView.setAlpha(1f);
            ImgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_Crafting_List.class);
                }
            });
            Title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_Crafting_List.class);
                }
            });
        } else {
            //ensure all fields are enabled
            Title.setEnabled(false);
            Title.setVisibility(View.VISIBLE);

            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.VISIBLE);

            Title.setOnClickListener(null);
            ImgView.setOnClickListener(null);

            ImgView.setAlpha(.2f);
            Title.setAlpha(.2f);
        }
    }
    private void SetupEnabledTutorial(TextView Title, TextView comingSoon, ImageView ImgView, Boolean value) {
        if(value){
            Title.setEnabled(true);
            Title.setVisibility(View.VISIBLE);
            Title.setAlpha(1f);
            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.INVISIBLE);
            comingSoon.setAlpha(1f);
            ImgView.setAlpha(1f);
            ImgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
            Title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
        } else {
            //ensure all fields are enabled
            Title.setEnabled(false);
            Title.setVisibility(View.VISIBLE);

            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.VISIBLE);

            Title.setOnClickListener(null);
            ImgView.setOnClickListener(null);

            ImgView.setAlpha(.2f);
            Title.setAlpha(.2f);
        }
    }
    private void SetupEnabledAccount(TextView Title, TextView comingSoon, ImageView ImgView, Boolean value) {
        if(value){
            Title.setEnabled(true);
            Title.setVisibility(View.VISIBLE);
            Title.setAlpha(1f);
            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.INVISIBLE);
            comingSoon.setAlpha(1f);
            ImgView.setAlpha(1f);
            ImgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
            Title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tm.intent(Activity_Home.this, Activity_World_List.class);
                }
            });
        } else {
            //ensure all fields are enabled
            Title.setEnabled(false);
            Title.setVisibility(View.VISIBLE);

            comingSoon.setEnabled(false);
            comingSoon.setVisibility(View.VISIBLE);

            Title.setOnClickListener(null);
            ImgView.setOnClickListener(null);

            ImgView.setAlpha(.2f);
            Title.setAlpha(.2f);
        }
    }
    //endregion

}
